---

title: Go Blog Part 4 - Making it Cross to the Docks
author: Andrew Colvin
date: February 2019

---

# Go Blog Part 4 -- Making it Cross to the Docks

## Introduction
Part 4 of the series is going to look at 3 areas of working with Go. 

- cross compiling for other operating systems
- using make to manage our build processes
- building and deploying in a container

As a recap the previous articles built a library, wrapped this with a rest service, looked at debugging and running unit tests.

## Cross Compilation
Cross compilation is a process of compiling and building on one architecture (platform and OS) for the end goal of running the units produces on a different architecture.  An example of this is building on a 64 bit x86 linux platform for a 64 bit MS Windows x86 platform or even a Raspberry Pi.  A pretty powerful technique that provides the capability to have a central build process.

The `go` command (since version 1.1.5) has this cross compilation ability built in.  There are a few environment variables that control the architecture that `go` tries to build.  `Go` does need to have appropriate library versions installed if a program is linking into external OS libraries and maintaining a full toolchain for all these can become cumbersome e.g. when using QT and Go.

The Go documentation [tells](https://golang.org/doc/install/source#environment) us what platforms it can cross compile  to and the link provided is the best and official source.

The environment variables to define the compile platform are:

- GOOS
- GOARCH

The `go env` command will let us review our go environment variables used 

```shell
.../cmd/bloodrest> go env
GOARCH="amd64"
GOBIN=""
GOCACHE="/home/andrew/.cache/go-build"
GOEXE=""
GOFLAGS=""
GOHOSTARCH="amd64"
GOHOSTOS="linux"
GOOS="linux"
GOPATH="/home/andrew/go"
GOPROXY=""
GORACE=""
GOROOT="/usr/lib64/go/1.11"
GOTMPDIR=""
GOTOOLDIR="/usr/lib64/go/1.11/pkg/tool/linux_amd64"
GCCGO="gccgo"
CC="gcc"
CXX="g++"
CGO_ENABLED="1"
GOMOD=""
CGO_CFLAGS="-g -O2"
CGO_CPPFLAGS=""
CGO_CXXFLAGS="-g -O2"
CGO_FFLAGS="-g -O2"
CGO_LDFLAGS="-g -O2"
PKG_CONFIG="pkg-config"
GOGCCFLAGS="-fPIC -m64 -pthread -fmessage-length=0 -fdebug-prefix-map=/tmp/go-build093297664=/tmp/go-build -gno-record-gcc-switches"

```

The Go OS `GOOS="linux"` and  Go Architecture `GOARCH="amd64"` specify that all of the builds completed to date using `go build` have been for linux on x86/64 platform 

Using these variables it should now be easy to build our rest service for windows.  Looking at the permissible combinations of `GOOS` and `GOARCH` that is appropriate for windows gives `windows/386` and `windows/amd64`

Now that we are armed with the above it is easy to compile for 64bit Windows server.  I will add the `-v` flag so it is more verbose 

```bash
.../bloodglucose/blood> GOOS=windows  GOARCH=amd64 go build -v  ./cmd/bloodrest
errors
internal/race
internal/syscall/windows/sysdll
runtime/internal/sys
internal/cpu
runtime/internal/atomic
unicode/utf16
sync/atomic
unicode/utf8
math/bits
unicode
encoding
internal/testlog
container/list
crypto/internal/subtle
crypto/subtle
vendor/golang_org/x/crypto/cryptobyte/asn1
internal/bytealg
math
vendor/golang_org/x/net/dns/dnsmessage
internal/nettrace
vendor/golang_org/x/crypto/curve25519
runtime
strconv
crypto/rc4
sync
internal/singleflight
io
math/rand
reflect
syscall
crypto/internal/randutil
hash
bytes
strings
crypto/cipher
hash/crc32
crypto
crypto/hmac
crypto/sha512
crypto/md5
crypto/aes
bufio
crypto/sha1
crypto/sha256
vendor/golang_org/x/text/transform
path
internal/syscall/windows/registry
internal/syscall/windows
time
encoding/binary
sort
regexp/syntax
encoding/base64
vendor/golang_org/x/crypto/poly1305
crypto/des
vendor/golang_org/x/crypto/internal/chacha20
internal/poll
encoding/pem
vendor/golang_org/x/crypto/chacha20poly1305
os
regexp
os/signal
path/filepath
fmt
io/ioutil
context
flag
encoding/hex
log
net/url
encoding/json
compress/flate
math/big
vendor/golang_org/x/text/unicode/bidi
vendor/golang_org/x/text/unicode/norm
net
vendor/golang_org/x/net/http2/hpack
mime
mime/quotedprintable
vendor/golang_org/x/text/secure/bidirule
net/http/internal
compress/gzip
github.com/emirpasic/gods/utils
github.com/emirpasic/gods/containers
github.com/emirpasic/gods/sets
github.com/emirpasic/gods/trees
database/sql/driver
github.com/emirpasic/gods/trees/redblacktree
vendor/golang_org/x/net/idna
crypto/elliptic
encoding/asn1
crypto/dsa
crypto/rand
github.com/emirpasic/gods/sets/treeset
crypto/rsa
github.com/segmentio/ksuid
crypto/ecdsa
vendor/golang_org/x/crypto/cryptobyte
crypto/x509/pkix
gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood
vendor/golang_org/x/net/http/httpproxy
net/textproto
crypto/x509
vendor/golang_org/x/net/http/httpguts
mime/multipart
crypto/tls
net/http/httptrace
net/http
github.com/gorilla/mux

```

The -v shows all of the libraries that Go has compiled for the target architecture and these are saved into the `GOCACHE` location which can be seen above.  Without the `-v` or running it again will give none of this output as it is now cached 

Looking into the directory shows the bloodrest.exe file we have just built

```bash
...go/bloodglucose/blood> ls -l
total 7916
-rw-r--r-- 1 andrew users    2516 Feb 16 21:21 A_Colvin_CA_version_2.crt
drwxr-xr-x 1 andrew users       0 Feb 16 18:43 bin
-rwxr-xr-x 1 andrew users 7126528 Feb 17 10:51 bloodrest.exe
drwxr-xr-x 1 andrew users      36 Feb  1 05:50 cmd
drwxr-xr-x 1 andrew users      52 Feb 17 08:11 docker
-rw-r--r-- 1 andrew users   40397 Feb  1 05:50 go_blog1.md
-rw-r--r-- 1 andrew users  334317 Feb  1 05:50 go_blog1.odt
-rw-r--r-- 1 andrew users   18661 Feb  1 05:50 go_blog2.1.md
-rw-r--r-- 1 andrew users   51829 Feb  1 05:50 go_blog2.md
-rw-r--r-- 1 andrew users  207464 Feb  1 05:50 go_blog2.odt
-rw-r--r-- 1 andrew users   37333 Feb  4 18:31 go_blog3.md
-rw-r--r-- 1 andrew users   32278 Feb  4 20:17 go_blog3.odt
-rw-r--r-- 1 andrew users  222466 Feb  4 20:13 go_blog3.pdf
-rw-r--r-- 1 andrew users    2814 Feb 17 10:45 go_blog4.md
-rw-r--r-- 1 andrew users    2748 Feb 17 10:45 go_blog4.md.backup
-rw-r--r-- 1 andrew users    1188 Feb 14 16:14 Makefile
drwxr-xr-x 1 andrew users     242 Feb  1 05:50 Pictures
drwxr-xr-x 1 andrew users      28 Feb  1 05:50 pkg
-rw-r--r-- 1 andrew users     812 Feb 11 07:23 README.md

```

Using the `file` command to inspect the file shows

```bash

...go/bloodglucose/blood> file bloodrest.exe 
bloodrest.exe: PE32+ executable (console) x86-64 (stripped to external PDB), for MS Windows
```

If we try an execute it in the shell we will get an error

```bash
...go/bloodglucose/blood> ./bloodrest.exe
bash: ./bloodrest.exe: cannot execute binary file: Exec format error

```

However, all is not lost as I have [wine](https://www.winehq.org/) installed that provides an execution route

```bash
...go/bloodglucose/blood> wine bloodrest.exe
017d:fixme:process:SetProcessPriorityBoost (0xffffffffffffffff,1): stub
2019/02/17 10:59:58 Blood Rest Server listening on :9001

``` 

Apart from the `fixme` which is immaterial the output looks the same as in blog 3 previously.  Running our test `curl` commands

```bash
...go/bloodglucose/blood/cmd/bloodrest> . ./blood-rest-tests.sh 
{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id01"}
{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id02"}
{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}

get all

[{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id01"},{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id02"},{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}]


{"error": "A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}

get all

[{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id01"},{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id02"},{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}]


{"error": "A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}
{"error": "A reading already exists at 2019-01-25T07:59:00Z with the ID id02"}
get all
[{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id01"},{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id02"},{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}]


```

This is the expected output from the shell and the log output from the rest service is also as expected.  

```
...go/bloodglucose/blood> wine bloodrest.exe
017d:fixme:process:SetProcessPriorityBoost (0xffffffffffffffff,1): stub
2019/02/17 10:59:58 Blood Rest Server listening on :9001
2019/02/17 11:02:09 params=map[id:id01]
2019/02/17 11:02:09 ID in url and body not the same, overriding the body: id01 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 11:02:09 params=map[id:id02]
2019/02/17 11:02:09 ID in url and body not the same, overriding the body: id02 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 11:02:09 params=map[id:id03]
2019/02/17 11:02:09 ID in url and body not the same, overriding the body: id03 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 11:02:09 creating reading with id and date id03, 2019-01-25T08:59:00Z
2019/02/17 11:02:09 A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg
2019/02/17 11:02:09 creating reading with id and date id04, 2019-01-25T08:59:00Z
2019/02/17 11:02:09 A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg
2019/02/17 11:02:09 creating reading with id and date id03, 2019-01-25T07:59:00Z
2019/02/17 11:02:09 A reading already exists at 2019-01-25T07:59:00Z with the ID id02

```

Exiting our service with `ctrl-c` also shows a clean shutdown

```
^C2019/02/17 11:04:27 Interrupt Received
2019/02/17 11:04:27 Blood Rest Server Shutdown
```

To clean out out go cache and we can use the `go clean` command.  `go clean` on its own just removes built packages etc where `go clean -cache` will clear out our cache which normally will not be needed.

### Amazing

As demonstrated this all looks super easy and the list of opportunities for cross compilation are great.  If you have ever set up tool chains before for other languages you will understand why this subsection is just called "amazing"

## Simplifying the Build Processes

Go and its tools make the processes of building anything easy using the `go` command so why is this section called simplifying? To date we have looked at building the library, running tests, cross compiling, building debug versions, building the rest service etc.  This is quite a few things so wouldn't it be easier to have simple standard jobs.  I could write a shell script that just does what I need but for many years the unix family has a tool to do just this called `make`.  There are many discussions on the internet about Go not needing make and yes this is true as we have seen but we need something to help manage our build pipeline.  Due to this there is even a tool called `mage` which can replace makefiles with go functions to execute your pipeline.  

Another option to manage our pipeline is to rely on Jenkins.  Jenkins has a Go plugin that will create a Go build environment.  This can be couple with Jenkins agents to keep the build clean.  If you configure these agents to run on a kubernetes (instructions how to connect jenkins and  kubernetes are in my kubenetes blog)or docker swarm cluster then we have a dynamic/clean room build environment.  I am not going to look at this here but later on we will look at building inside a docker container and also building a docker container with our REST service both of which could form the back bone of a pipeline.

### Make

For those of you that have never seen make or used it then you are in for a surprise.  Like all things each generation think they do things better than their predecessors.  Make is a build automation tool that was first created in 1976 (yes that is over 40 years ago) and is still extremely capable and powerful and many of the OS and tools you all use are built using `make` still.  Why?  Well if it ain't broke don't fix it.  As an aside even visual studio has nmake (a make variant) built in under the covers.

It is the author of the Makefiles responsibility to define dependencies between targets so that if you change a file `make` can work out what needs to be recompiled without having to build the world.   As such and because people are lazy there are tools such as `configure` and `automake` to help out.  To read more about the capabilities of make see [the wiki page](https://en.wikipedia.org/wiki/Make_(software)) as a simple start point or read make's manual with `man make` for basic options and for a full manual use `info make`

```
Next: Overview,  Prev: (dir),  Up: (dir)

GNU 'make'
**********

This file documents the GNU 'make' utility, which determines
automatically which pieces of a large program need to be recompiled, and
issues the commands to recompile them.

   This is Edition 0.74, last updated 21 May 2016, of 'The GNU Make
Manual', for GNU 'make' version 4.2.1.

   Copyright (C) 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996,
1997, 1998, 1999, 2000, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009,
2010, 2011, 2012, 2013, 2014, 2015, 2016 Free Software Foundation, Inc.

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU Free Documentation License,
     Version 1.3 or any later version published by the Free Software
     Foundation; with no Invariant Sections, with the Front-Cover Texts
     being "A GNU Manual," and with the Back-Cover Texts as in (a)
     below.  A copy of the license is included in the section entitled
     "GNU Free Documentation License."

     (a) The FSF's Back-Cover Text is: "You have the freedom to copy and
     modify this GNU manual.  Buying copies from the FSF supports it in
     developing GNU and promoting software freedom."

* Menu:

* Overview::                    Overview of 'make'.
* Introduction::                An introduction to 'make'.
* Makefiles::                   Makefiles tell 'make' what to do.
* Rules::                       Rules describe when a file must be remade.
* Recipes::                     Recipes say how to remake a file.
* Using Variables::             You can use variables to avoid repetition.
* Conditionals::                Use or ignore parts of the makefile based
                                  on the values of variables.
* Functions::                   Many powerful ways to manipulate text.
* Invoking make: Running.       How to invoke 'make' on the command line.
* Implicit Rules::              Use implicit rules to treat many files alike,
                                  based on their file names.
* Archives::                    How 'make' can update library archives.
* Extending make::              Using extensions to 'make'.
* Integrating make::            Integrating 'make' with other tools.
-----Info: (make)Top, 311 lines --Top----------------------------------------------------------------------------------------------------------------------------------
Unknown command (`)

```


For those of you scared of the command line you can find it in a webpage [here](https://www.gnu.org/software/make/manual/make.html)

#### Makefile

To use make we need to define a `Makefile` with a set of targets.  Each target has a set of dependencies and a set of actions to complete the target.  It surely can't be simpler

A simple example of target `build` is dependent of `test` both of which need external libraries to be fetch with `go get` We can already see our chain building.  Before, anything is completed we need to ensure we have any directory structure necessary to facilitate our targets

Starting with the directories is a very simple example.  As we already have our git repo we only need a location to build the executable into 

```make
dir:
        mkdir -p $(BINARY_DIR)

```

With this target defined it can be actioned using `make dir` and we set `BINARY_DIR` variable at the top of the file.  If necessary we could build a target that does the complete download with git so that the full setup could be completed from the Makefile.

The target is defined using the following format

```make
<target name>: <comma separated list of dependencies>
<----tab----><command and args>

```

Note the `tab`, this is important and it will not work with spaces

The follow siple targets are defined and there purpose described 

- dir -- create the directory structure necessary
- deps -- download the dependencies for the blood package.  Note `-u` forces download each time
- debug -- build the code with debugging symbols
- test -- This is dependent on fetching the dependencies and runs the go test files
- build -- builds the service and package if and only if the test target is successful
 
```make

dir:
        mkdir -p $(BINARY_DIR)

deps:
        cd ./pkg/blood && \
        go get -u
        cd ./cmd/bloodrest && \
        go get -u

debug:
        cd ./pkg/blood && \
        go build -gcflags='-N -l'
        cd ./cmd/bloodrest && \
        go build -gcflags='-N -l'

test: deps
        cd ./pkg/blood && \
        go test

build:  test
        cd ./pkg/blood && \
        go build
        cd ./cmd/bloodrest && \
        go build
```

Now that code is being built we need the ability to install into the location we require it and to also be able to clean the builds out

```make
install: build
        cd ./pkg/blood && \
        go install
        cd ./cmd/bloodrest && \
        go install

clean:
        cd ./pkg/blood && \
        go clean -i
        cd ./cmd/bloodrest && \
        go clean -i && rm -f $(BINARY_DIR)/$(BINARY_NAME)-*64*

```

We have set install to be dependent on build but this isn't necessarily true as `go install` will run build for itself.  

Now let us add our cross compile targets so that the Makefile offers building for linux, windows or Raspberry Pi

```make
build-linux: test dir
        cd ./cmd/bloodrest && \
        GOOS=linux GOARCH=amd64 go build -o $(BINARY_DIR)/$(BINARY_NAME)-amd64

build-pi: test dir
        cd ./cmd/bloodrest && \
        GOOS=linux  GOARCH=arm64 go build -o $(BINARY_DIR)/$(BINARY_NAME)-arm64

build-windows: test dir
        cd ./cmd/bloodrest && \
        GOOS=windows  GOARCH=amd64 go build -o $(BINARY_DIR)/$(BINARY_NAME)-win64.exe
        
```

Finally, make if given no targets tries to make the target `all`.  Therefore it is sensible to define the target `all` which will do our default pipeline process.  

```make
all:    install build-all

build-all: build-linux build-pi build-windows

.phony: install test build deps all clean build-pi build-windows build-linux dir debug build-all
```

The `.phoney` target is just there to tell make that these targets are not files that it needs to track for change and provides for a a file to be created with the same name as the target.

Running `make` produces the following output and the executables created in the bin directory and because the install target was executed the bloodrest service was also built for the local target and installed into the default go bin location `$GOPATH/bin`

```bash
andrew@host:~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood> make
cd ./pkg/blood && \
go get -u
cd ./cmd/bloodrest && \
go get -u
cd ./pkg/blood && \
go test
PASS
ok      gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood    0.002s
cd ./pkg/blood && \
go build
cd ./cmd/bloodrest && \
go build
g macd ./pkg/blood && \
go install
cd ./cmd/bloodrest && \
go install
mkdir -p /home/andrew/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bin
cd ./cmd/bloodrest && \
GOOS=linux GOARCH=amd64 go build -o /home/andrew/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bin/bloodrest-amd64
cd ./cmd/bloodrest && \
GOOS=linux  GOARCH=arm64 go build -o /home/andrew/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bin/bloodrest-arm64
cd ./cmd/bloodrest && \
GOOS=windows  GOARCH=amd64 go build -o /home/andrew/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bin/bloodrest-win64.exe


andrew@host:~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood> ls bin
bloodrest-amd64  bloodrest-arm64  bloodrest-win64.exe


```

## Containerisation

### Building within a Container

If we require a "clean room" to build the service or wish to push the job off into a build cloud then we can use a docker container to do this.  There are multiple official golang images on dockerhub based on various underlying base builds.  

To use the standard we are going to make use of golang:latest.  This is a Debian Stretch image with go, git, make and many other tools required to support Go.

To complete the build it is easy to add our go environment as a volume into the docker runtime and use our make command.  There is one complication:  my gitlab server makes use of a private Certificate Authority which is available from [download](http://www.acolvin.me.uk/acolvinCA2.pem) but also included in the repo in .crt format.  It is possible to build a new image including this certificate but for this simple case we can tell git to ignore ssl verification.

The basis of the command will be    
>`docker run golang:latest make` 

This tells docker to download the Golang:latest image and create and run a container with this image executing the make command.  The container has no code yet so this alone isn't enough


As it is sensible to clean up our used runtimes add `--rm` to remove the stopped and finished with containers.  Additionally, we need to interact with the container so add `-i -t` often abbreviated to `-it` meaning interactive and attach a pseudo tty

We haven't actually added our code into the container so let's now mount the volume  using `-v /home/andrew/go:/go` which mounts my GOPATH into the container at `/go` I could have use `$GOPATH` in this case but be careful as the GOPATH can hold multiple locations and this then wouldn't work.

Set the working location to be that the base directory of our code in the container `-w /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood`

Execute both the git command and the make command `bash -c 'git config --local http.sslVerify "false" && make build'`

Bringing it all together as a single command


```
docker run --rm -it -v /home/andrew/go:/go -w /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood  golang:latest  bash -c 'git config --local http.sslVerify "false" && make build '
```


```bash
andrew@host:~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood> docker run --rm -it -v /home/andrew/go:/go -w /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood  golang:latest  bash -c 'git config --local http.sslVerify "false" && make build '
cd ./pkg/blood && \
go get -u
cd ./cmd/bloodrest && \
go get -u
cd ./pkg/blood && \
go test
PASS
ok      gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood    0.003s
cd ./pkg/blood && \
go build
cd ./cmd/bloodrest && \
go build

```

Where is the build (note that my user is configured with rights to run docker) as I have added it to the docker group.  


```bash
andrew@host:~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood> ll cmd/bloodrest/
total 7256
-rwxr-xr-x 1 root   root  7405603 Feb 17 17:09 bloodrest
-rw-r--r-- 1 andrew users    8461 Feb  4 18:31 bloodrest.go
-rw-r--r-- 1 andrew users    1222 Feb  1 11:19 blood-rest-tests.sh
-rw-r--r-- 1 andrew users      57 Feb  1 05:50 README.md

```

Just where you would expect... Just one complication our docker command has created the executable but it is owned by root.  I can still `clean` this as the directory is owned by me 

Now that it is obviously easy to compile our service using a docker container we could easily create a jenkins agent to do this as part of a pipeline process running test and then build and finally install.  

### Deploying the Service in a Container

The last section built the service using a container but didn't actually leave the container as the implementation to run the service.  This part will develop this build process to create a container with the service as the run target of the image
 
Docker images are often defined using a dockerfile (a container build definition) and to keep our directory clean we will build this in a directory `docker` (see the code repo)

Create a dockerfile called `bloodrest.build.dockerfile` and declare that it is based on the golang image

```dockerfile
FROM golang:latest

```

Now make the directories required.  RUN executes a command as part of the image build process

```dockerfile

RUN mkdir -p /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
RUN mkdir -p /go/pkg
RUN mkdir -p /go/bin

```

It is not safe to run our service as root (the default) so it is necessary to create a user.  A docker process running as root which has access to host resources has root access to those host resources.  

```dockerfile
RUN adduser --system --home /go --no-create-home --disabled-password appuser

```

This time we are not going to mount the volume into the container but copy the code into the container so it is self contained.  This assumes we are building the container from our repo's root

```dockerfile
ADD  . /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood

```

Set the working directory within the container

```dockerfile
WORKDIR /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood

```

Change the ownership of all the files to our `appuser` created above
```dockerfile
RUN chown -R appuser /go

```

Now we are almost complete with the preparation but need to add the custom CA

```dockerfile
COPY ./A_Colvin_CA_version_2.crt /usr/local/share/ca-certificates/A_Colvin_CA_version_2.crt
RUN update-ca-certificates
```

Now the image build is ready to drop down to our user and do the build

```dockerfile
USER appuser
ENV GOPATH /go
RUN ls -l

RUN go get ./...
RUN go build -o bloodrest ./cmd/bloodrest/bloodrest.go
RUN ls -l

CMD ["./bloodrest"]

```

I have added the `ls -l` commands so that we can see the executable after creation (for the purpose of the blog. 

Finally define the docker CMD to run as the service built when the container is created.

The following line will build the image ready to be deployed `-t ... ` tags the image so that it can be referred to as part of any `docker run` command

```docker
docker build -t bloodrest:0.1 . -f docker/bloodrest.build.dockerfile

Step 1/17 : FROM golang:latest
 ---> 901414995ecd
Step 2/17 : RUN mkdir -p /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
 ---> Using cache
 ---> 55e013373332
Step 3/17 : RUN mkdir -p /go/pkg
 ---> Using cache
 ---> ef98848b371a
Step 4/17 : RUN mkdir -p /go/bin
 ---> Using cache
 ---> 1f045b0a67d6
Step 5/17 : RUN adduser --system --home /go --no-create-home --disabled-password appuser
 ---> Using cache
 ---> 24ce79c11139
Step 6/17 : ADD  . /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
 ---> e5120276d5de
Step 7/17 : WORKDIR /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
 ---> Running in cfd9028a2f05
Removing intermediate container cfd9028a2f05
 ---> c01f03e1f7e1
Step 8/17 : RUN chown -R appuser /go
 ---> Running in 19551c1806a3
Removing intermediate container 19551c1806a3
 ---> f25da48e7567
Step 9/17 : COPY ./A_Colvin_CA_version_2.crt /usr/local/share/ca-certificates/A_Colvin_CA_version_2.crt
 ---> 45706145a361
Step 10/17 : RUN update-ca-certificates
 ---> Running in cac748c5b8c7
Updating certificates in /etc/ssl/certs...
1 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
Removing intermediate container cac748c5b8c7
 ---> af85588cf3d5
Step 11/17 : USER appuser
 ---> Running in 432eb10ee971
Removing intermediate container 432eb10ee971
 ---> 78c6b139d1bf
Step 12/17 : ENV GOPATH /go
 ---> Running in 9dbb2c193ed3
Removing intermediate container 9dbb2c193ed3
 ---> 5c976d928a14
Step 13/17 : RUN ls -l
 ---> Running in ae8cc28e9ebe
total 1004
-rw-r--r-- 1 appuser root   2516 Feb 16 21:21 A_Colvin_CA_version_2.crt
-rw-r--r-- 1 appuser root   1188 Feb 14 16:14 Makefile
drwxr-xr-x 1 appuser root    242 Feb  1 05:50 Pictures
-rw-r--r-- 1 appuser root    812 Feb 11 07:23 README.md
drwxr-xr-x 1 appuser root      0 Feb 17 17:15 bin
drwxr-xr-x 1 appuser root     36 Feb  1 05:50 cmd
drwxr-xr-x 1 appuser root     52 Feb 17 08:11 docker
-rw-r--r-- 1 appuser root  40397 Feb  1 05:50 go_blog1.md
-rw-r--r-- 1 appuser root 334317 Feb  1 05:50 go_blog1.odt
-rw-r--r-- 1 appuser root  18661 Feb  1 05:50 go_blog2.1.md
-rw-r--r-- 1 appuser root  51829 Feb  1 05:50 go_blog2.md
-rw-r--r-- 1 appuser root 207464 Feb  1 05:50 go_blog2.odt
-rw-r--r-- 1 appuser root  37333 Feb  4 18:31 go_blog3.md
-rw-r--r-- 1 appuser root  32278 Feb  4 20:17 go_blog3.odt
-rw-r--r-- 1 appuser root 222466 Feb  4 20:13 go_blog3.pdf
-rw-r--r-- 1 appuser root  26852 Feb 17 17:56 go_blog4.md
-rw-r--r-- 1 appuser root  26842 Feb 17 17:56 go_blog4.md.backup
drwxr-xr-x 1 appuser root     28 Feb  1 05:50 pkg
Removing intermediate container ae8cc28e9ebe
 ---> 885899560c9a
Step 14/17 : RUN go get ./...
 ---> Running in 7e6b9d92381d
Removing intermediate container 7e6b9d92381d
 ---> 31145debc7b9
Step 15/17 : RUN go build -o bloodrest ./cmd/bloodrest/bloodrest.go
 ---> Running in 755280e03b07
Removing intermediate container 755280e03b07
 ---> 215a11b3da88
Step 16/17 : RUN ls -l
 ---> Running in 9501afeb4892
total 8240
-rw-r--r-- 1 appuser root       2516 Feb 16 21:21 A_Colvin_CA_version_2.crt
-rw-r--r-- 1 appuser root       1188 Feb 14 16:14 Makefile
drwxr-xr-x 1 appuser root        242 Feb  1 05:50 Pictures
-rw-r--r-- 1 appuser root        812 Feb 11 07:23 README.md
drwxr-xr-x 1 appuser root          0 Feb 17 17:15 bin
-rwxr-xr-x 1 appuser nogroup 7405603 Feb 17 17:58 bloodrest
drwxr-xr-x 1 appuser root         36 Feb  1 05:50 cmd
drwxr-xr-x 1 appuser root         52 Feb 17 08:11 docker
-rw-r--r-- 1 appuser root      40397 Feb  1 05:50 go_blog1.md
-rw-r--r-- 1 appuser root     334317 Feb  1 05:50 go_blog1.odt
-rw-r--r-- 1 appuser root      18661 Feb  1 05:50 go_blog2.1.md
-rw-r--r-- 1 appuser root      51829 Feb  1 05:50 go_blog2.md
-rw-r--r-- 1 appuser root     207464 Feb  1 05:50 go_blog2.odt
-rw-r--r-- 1 appuser root      37333 Feb  4 18:31 go_blog3.md
-rw-r--r-- 1 appuser root      32278 Feb  4 20:17 go_blog3.odt
-rw-r--r-- 1 appuser root     222466 Feb  4 20:13 go_blog3.pdf
-rw-r--r-- 1 appuser root      26852 Feb 17 17:56 go_blog4.md
-rw-r--r-- 1 appuser root      26842 Feb 17 17:56 go_blog4.md.backup
drwxr-xr-x 1 appuser root         28 Feb  1 05:50 pkg
Removing intermediate container 9501afeb4892
 ---> d494db2017a0
Step 17/17 : CMD ["./bloodrest"]
 ---> Running in 0e404b522bf5
Removing intermediate container 0e404b522bf5
 ---> fd875f15db54
Successfully built fd875f15db54
Successfully tagged bloodrest:0.1


```

Execution of our image and hence the service requires the execution of a container based on this image. The `-p 9001:9001` exposes the container port 9001 to the docker host

```
> docker run -it -p 9001:9001 --rm bloodrest:0.1
2019/02/17 18:00:14 Blood Rest Server listening on :9001

```

Additionally, the `docker run` command can pass environment variables into the container using the `--env` command line option to `docker run` but this isn't really necessary in this case as we can change our `-p 9001:9001` to map this to a specific host ip and port

#### Image Size and Security

The image created is based on the golang:latest which is built on top of debian stretch image.  As such it is fairly large in size

```
> docker images
REPOSITORY                       TAG                 IMAGE ID            CREATED             SIZE
bloodrest                        0.1                 fd875f15db54        37 minutes ago      840MB

``` 

We could change our base image to that built on Golang:Alpine which is a much smaller base image.  However, the alpine image does not have `git` so we need to add this or restructure our program to use `vendor` packages (see the Go documentation).

Replace `From golang:latest` with

```
FROM golang:alpine
RUN apk update && apk add git && rm -rf /var/cache/apk/*

``` 

>>  see dockerfile: bloodrest.build.alpine.dockerfile

Building the image gives a much smaller size (version 0.2) with an image that is only 40% of the original

```
> docker images
REPOSITORY                       TAG                 IMAGE ID            CREATED             SIZE
bloodrest                        0.2                 91e8cd5a66cd        18 seconds ago      350MB
bloodrest                        0.1                 fd875f15db54        About an hour ago   840MB
```

#### Cleaning up the Docker Image

The image we have built is based on the golang image.  This includes all of the development tools and is not the best image to use in a production position as any vulnerability could provide an attacker with a much better landing opportunity.  Therefore it is necessary to look at cleaning this image

Docker provides a great way to do this and that is to build the container inside the container or more precisely build in one image and generate another image from within this image as the final position.  Because the build container is not our deploy container it is not necessary to change users during the build

The dockerfile needs to be split into 2 sections the service build and then the image build.

>> see dockerfile: `bloodrest.build.alpine.clean.dockerfile`

```
FROM golang:alpine as build
RUN apk update && apk add git && rm -rf /var/cache/apk/*

RUN mkdir -p /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
RUN mkdir -p /go/pkg
RUN mkdir -p /go/bin

COPY ./A_Colvin_CA_version_2.crt /usr/local/share/ca-certificates/A_Colvin_CA_version_2.crt
RUN update-ca-certificates


ADD  . /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
WORKDIR /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood



ENV GOPATH /go


RUN go get ./...
RUN go build -o bloodrest ./cmd/bloodrest/bloodrest.go

From alpine

RUN adduser --system --home /app --no-create-home --disabled-password appuser 
USER appuser

COPY --from=build /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bloodrest /app/

WORKDIR /app
CMD ["./bloodrest"]
```


Building the new image (tagged as version 0.3)

```
> docker build -t bloodrest:0.3 . -f docker/bloodrest.build.alpine.clean.dockerfile
Sending build context to Docker daemon  3.206MB
Step 1/18 : FROM golang:alpine as build
 ---> cb1c8647572c
Step 2/18 : RUN apk update && apk add git && rm -rf /var/cache/apk/*
 ---> Using cache
 ---> c91db6518f10
Step 3/18 : RUN mkdir -p /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
 ---> Using cache
 ---> b32815413733
Step 4/18 : RUN mkdir -p /go/pkg
 ---> Using cache
 ---> 54292b53aa71
Step 5/18 : RUN mkdir -p /go/bin
 ---> Using cache
 ---> 3209fe3ca966
Step 6/18 : COPY ./A_Colvin_CA_version_2.crt /usr/local/share/ca-certificates/A_Colvin_CA_version_2.crt
 ---> Using cache
 ---> 201eb487b7fc
Step 7/18 : RUN update-ca-certificates
 ---> Using cache
 ---> 8da85769d7b3
Step 8/18 : ADD  . /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
 ---> 10bf61906dca
Step 9/18 : WORKDIR /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
 ---> Running in d5f0771429dd
Removing intermediate container d5f0771429dd
 ---> 74d10387dcc5
Step 10/18 : ENV GOPATH /go
 ---> Running in ee483f700356
Removing intermediate container ee483f700356
 ---> 70bfc43023ec
Step 11/18 : RUN go get ./...
 ---> Running in c1ebd6df3bef
Removing intermediate container c1ebd6df3bef
 ---> 236af2d996f1
Step 12/18 : RUN go build -o bloodrest ./cmd/bloodrest/bloodrest.go
 ---> Running in db87b4740de8
Removing intermediate container db87b4740de8
 ---> f2f1514f920c
Step 13/18 : From alpine
 ---> caf27325b298
Step 14/18 : RUN adduser --system --home /app --no-create-home --disabled-password appuser
 ---> Running in 6f45d26cbb75
Removing intermediate container 6f45d26cbb75
 ---> 62da19bc8992
Step 15/18 : USER appuser
 ---> Running in e6498dcc0970
Removing intermediate container e6498dcc0970
 ---> 107bb80fe9a9
Step 16/18 : COPY --from=build /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bloodrest /app/
 ---> 36caa96a194b
Step 17/18 : WORKDIR /app
 ---> Running in 8af5c6a0a7b0
Removing intermediate container 8af5c6a0a7b0
 ---> 9eb8d99e601b
Step 18/18 : CMD ["./bloodrest"]
 ---> Running in 1d4621454a81
Removing intermediate container 1d4621454a81
 ---> 3e3bd093e191
Successfully built 3e3bd093e191
Successfully tagged bloodrest:0.3

```

Executing the image to prove it works

```
> docker run  -p 9001:9001 --rm bloodrest:0.3
2019/02/17 19:05:56 Blood Rest Server listening on :9001
2019/02/17 19:06:17 params=map[id:id01]
2019/02/17 19:06:17 ID in url and body not the same, overriding the body: id01 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 19:06:17 params=map[id:id02]
2019/02/17 19:06:17 ID in url and body not the same, overriding the body: id02 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 19:06:17 params=map[id:id03]
2019/02/17 19:06:17 ID in url and body not the same, overriding the body: id03 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 19:06:17 creating reading with id and date id03, 2019-01-25T08:59:00Z
2019/02/17 19:06:17 A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg
2019/02/17 19:06:17 creating reading with id and date id04, 2019-01-25T08:59:00Z
2019/02/17 19:06:17 A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg
2019/02/17 19:06:17 creating reading with id and date id03, 2019-01-25T07:59:00Z
2019/02/17 19:06:17 A reading already exists at 2019-01-25T07:59:00Z with the ID id02
2019/02/17 19:08:39 Interrupt Received
2019/02/17 19:08:39 Blood Rest Server Shutdown

```

Just how small is this new image

```
> docker images
REPOSITORY                       TAG                 IMAGE ID            CREATED             SIZE
bloodrest                        0.3                 3e3bd093e191        5 minutes ago       12.9MB
bloodrest                        0.2                 91e8cd5a66cd        26 minutes ago      350MB
bloodrest                        0.1                 fd875f15db54        About an hour ago   840MB

```

A real reduction in size which only has the very basic alpine OS at its core and therefore provides a much smaller attack vector

There is an opportunity to do better.  For this it is necessary to statically compile our service. A static compile means "build and link everything needed into the executable so it relies on nothing external". This is achieved with the following command line

```
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' cmd/bloodrest/bloodrest.go

```

Now change the dockerfile so that it creates the image from scratch (i.e. no OS in the container).  Given that there is no OS we cannot change user

>> see dockerfile: `bloodrest.build.scratch.dockerfile`

We add a second From instruction and copy from the build image (which we named build)  

```
Step 13/16 : From scratch
 ---> 
Step 14/16 : COPY --from=build /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bloodrest /app/
 ---> 8a1369b394f1
Step 15/16 : WORKDIR /app
 ---> Running in 909c54fc7388
Removing intermediate container 909c54fc7388
 ---> 37b9f8fa1d61
Step 16/16 : CMD ["./bloodrest"]
 ---> Running in 11cb87cb4f3e
Removing intermediate container 11cb87cb4f3e
 ---> 73de4d697fe3
Successfully built 73de4d697fe3
Successfully tagged bloodrest:0.4
```

Run the image and prove it works

```
andrew@host:~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood> docker run  -p 9001:9001 --rm bloodrest:0.4
2019/02/17 19:31:03 Blood Rest Server listening on :9001
2019/02/17 19:31:13 params=map[id:id01]
2019/02/17 19:31:13 ID in url and body not the same, overriding the body: id01 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 19:31:13 params=map[id:id02]
2019/02/17 19:31:13 ID in url and body not the same, overriding the body: id02 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 19:31:13 params=map[id:id03]
2019/02/17 19:31:13 ID in url and body not the same, overriding the body: id03 1GFRt5ojh3H5ObUAZ4cDDcDDgWg.  Setting reading to URL ID
2019/02/17 19:31:13 creating reading with id and date id03, 2019-01-25T08:59:00Z
2019/02/17 19:31:13 A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg
2019/02/17 19:31:13 creating reading with id and date id04, 2019-01-25T08:59:00Z
2019/02/17 19:31:13 A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg
2019/02/17 19:31:13 creating reading with id and date id03, 2019-01-25T07:59:00Z
2019/02/17 19:31:13 A reading already exists at 2019-01-25T07:59:00Z with the ID id02
^C2019/02/17 19:32:03 Interrupt Received
2019/02/17 19:32:03 Blood Rest Server Shutdown

```

and the image size...

```
> docker images
REPOSITORY                       TAG                 IMAGE ID            CREATED             SIZE
bloodrest                        0.4                 73de4d697fe3        11 minutes ago      7.35MB
bloodrest                        0.3                 3e3bd093e191        38 minutes ago      12.9MB
bloodrest                        0.2                 91e8cd5a66cd        About an hour ago   350MB
bloodrest                        0.1                 fd875f15db54        2 hours ago         840MB

```

This has, however generated an additional issue:

```
> ps aux | grep blood
andrew   32107  0.0  0.1 1111116 54972 pts/7   Sl+  19:38   0:00 docker run -p 9001:9001 --rm bloodrest:0.4
root     32158  0.0  0.0 106664  7488 ?        Ssl  19:38   0:00 ./bloodrest

``` 

The `./bloodrest` service is running as root on the host.  A way to fix this is to create the user in the build image and then copy the `passwd` file to the scratch based image 

>> dockerfile: bloodrest.build.scratch.noroot.dockerfile


```
FROM golang:alpine as build
RUN apk update && apk add git && rm -rf /var/cache/apk/*

RUN mkdir -p /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
RUN mkdir -p /go/pkg
RUN mkdir -p /go/bin

COPY ./A_Colvin_CA_version_2.crt /usr/local/share/ca-certificates/A_Colvin_CA_version_2.crt
RUN update-ca-certificates


ADD  . /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
WORKDIR /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood



ENV GOPATH /go


RUN go get ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o bloodrest ./cmd/bloodrest/bloodrest.go


RUN adduser --system --home /app --no-create-home --disabled-password appuser

From scratch

COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bloodrest /app/

USER appuser
WORKDIR /app
CMD ["./bloodrest"]

```

Building and running shows the process is no longer running as root

```
> ps aux | grep blood
andrew    5293  0.6  0.1 1037896 52076 pts/7   Sl+  19:55   0:00 docker run -p 9001:9001 --rm bloodrest:0.5
100       5353  0.5  0.0 108328  4532 ?        Ssl  19:55   0:00 ./bloodrest

```

## Recap and Conclusion

This blog looked into:

* cross compilation
* make
* building in containers
* creating and running a minimal execution container

Go makes cross compilation simple and is a great way to build programs for multiple platforms.  We need to keep in mind that external libraries (cgo etc) will introduce difficulties

Make provided us with an easy way to organise out pipeline and not have to remember all the different command line options to build debug versions etc.  Additionally a target can actually do a multi-platform build with one simple command

We looked at building inside a container so that this method could be used for pipelines in build systems

Finally we built the service inside a container for execution in the container.  This started with a full development capable container weighing in at 850MB and ended with a container that was a mere 8MB with no capabilities other than our executable.  

We have seen that Go, Docker and Make all sit together very nicely and with a little effort provides a great build platform for your programs.

