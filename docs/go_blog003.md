---

title: Go Blog Part 3 - Building the REST Service
author: Andrew Colvin
date: January 2019

---
# Building a REST Service 

In this article, the 4th of the series, we will look at using GO to create a rest service out of our data library we have written in the previous articles.  Here is a recap of the previous articles

- Part one: What is Go and how to start writing programs with it.
- Part two: Building more complex programs and structuring the directory, using the test framework.
- Part three: Debugging 

The blog will explore the basics of using an HTTP Router and requests in GO, demonstrating how to wrap our go to make the service function.  Our service will implement the following contracts

| URI | HTTP Action | Description | HTTP Response | HTTP Body |
|:----|:-----------:|:-----------:|:-------------:|:----------|
| `/bloods` | GET | Retrieve all Readings |200/500|JSON|  
| `/bloods` | POST | Store Reading and return stored value | 201/406 | JSON |
| `/blood/{id)` | PUT | Update or Store Reading with an ID value {id} | 201/202/406 | JSON |
| `/blood/{id}` | GET | Retrieve Reading with ID | 200/500 | JSON
| `/blood/{id}` | DELETE | Remove Reading with ID | 200/404 | empty |

The REST Service will be built so that it is a standalone executable which can eventually be packaged into a docker container.  Backing the service with a database will be investigated in a later article

## Main Application

In the previous articles we built libraries and test scripts but never built our application.  Our entry point of our application has to be in a package called main and the entry point is a function called main

```go

package main

import (
    ...
)

func main() {
    ...
}

```

We will create this file in the highlighted directory.  Note that the `pkg/blood` directory is the location of our library

> .../ **blood**   
> > **cmd**   
> > > **bloodrest**    
> > 
> > pkg   
> > > blood
   
Therefore create the file `blood/cmd/bloodrest/bloodrest.go` and add the `main` definition above.  The ellipses will be completed as we progress.

### Import the Blood Library

The REST service needs to load the library we have created.  This is achieved by adding the full path

```go
import (
    "gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood"
)


```
*Don't forget to change `gitlab.acolvin.me.uk/go/bloodglucose` to your location you have cloned the repository*

### Libraries Required

We are dealing with http server and JSON so we are going to need to investigate libraries for these.  We have already used a standard library for JSON and that is `encoding/json`.  

Go also provides a network package with an http sub-package and can be imported with `net/http`.  The documentation of the package can be found [here](https://golang.org/pkg/net/http/).  The overview from the link states: 
> Package http provides HTTP client and server implementations. 

If we are writing a server function it is likely that it will need to produce log messages.  Go comes with a `log` package which has basic functionality.  Its overview is given below
>Package log implements a simple logging package. It defines a type, Logger, with methods for formatting output. It also has a predefined 'standard' Logger accessible through helper functions Print[f|ln], Fatal[f|ln], and Panic[f|ln], which are easier to use than creating a Logger manually. That logger writes to standard error and prints the date and time of each logged message. Every log message is output on a separate line: if the message being printed does not end in a newline, the logger will add one. The Fatal functions call os.Exit(1) after writing the log message. The Panic functions call panic after writing the log message. 

Note that this is basic logging with no provision for levels or any of the other standard capabilities. What if more functionality is needed.  


The community is maintaining a great resource that needs to be on everyone's bookmark list if you are programming Go https://github.com/avelino/awesome-go 

This page provides a list of many differing logging frameworks from full log4j implementations to the simple glog which just adds log levels and was written and used by Google.  The most mentioned framework seems to be logrus which seems to be a complete all pervasive extension into the language. 

For this simple REST service we will stick to the standard `log`

The service being created will need to read and write to and from http streams so it is expected that there will be a need to import the input/output package `io`

To make life easier to build a full REST framework we could just pick up a complete framework from the awesome list but this article will just use a multiplexer framework so that our calls can be easily pattern matched and directed to an appropriate function.  A commonly used HTTP multiplexer is called mux and it is located at https://github.com/gorilla/mux

> Package gorilla/mux implements a request router and dispatcher for matching incoming requests to their respective handler.
> 
> The name mux stands for "HTTP request multiplexer". Like the standard http.ServeMux, mux.Router matches incoming requests against a list of registered routes and calls a handler for the route that matches the URL or other conditions. The main features are:
> 
>    It implements the http.Handler interface so it is compatible with the standard http.ServeMux.
    Requests can be matched based on URL host, path, path prefix, schemes, header and query values, HTTP methods or using custom matchers.
    URL hosts, paths and query values can have variables with an optional regular expression.
    Registered URLs can be built, or "reversed", which helps maintaining references to resources.
    Routes can be used as subrouters: nested routes are only tested if the parent route matches. This is useful to define groups of routes that share common conditions like a host, a path prefix or other repeated attributes. As a bonus, this optimizes request matching.


With format and time this is going to be our standard set 

```go
import (
    "encoding/json"
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood"
    "io"
    "time"
    "fmt"
)

```

## Getting Started with the Service

The first thing to do is create our http router and define the functions that are going to be called for each route.  The mux library we are using allows for routing based on host,uri, http method, to name a few.

To create the router use 
```go
 router := mux.NewRouter()
```

To assign actions to our router by URI we use the HandleFunc function. HandleFunc has the API 
```go 
func (r *Router) HandleFunc(path string, f func(http.ResponseWriter, *http.Request)) *Route
```
so the functions that need to be written have to meet this contract.  The path string is the definition of the uri matching that will be defined.  However, the router has to distinguish based on HTTP Method as well (`GET`,`POST`,`PUT`,`DELETE`).  The `router` also has a Methods function that allows this:
```go
func (r *Router) Methods(methods ...string) *Route

``` 

Note these functions take a `*Router` and return a `*Router` so are defined to be strung together.  

To define the get all route just requires:
```go
router.HandleFunc("/bloods", getReadings).Methods("GET")

```

This defines that an `HTTP GET` on `http://<host>:<port>/bloods` will call the function getReadings qhich needs to meet the contract
```go
func(http.ResponseWriter, *http.Request)
```
 
This one was simple in that it has no parameters.  Some of our REST definitions require that the ID be passed in the URL.  This is achieved by using `{}` in the `path` string, such as, `/blood/{id}` to match the definitions above.

Bringing it together

```go
func main() {
    // Create the http router
    router := mux.NewRouter()
    
    
    
    //This url allows query of a single reading
    router.HandleFunc("/blood/{id}", getReading).Methods("GET")
    
        
    //Pattern to create a reading 
    router.HandleFunc("/bloods", createReadingPOST).Methods("POST")
    
    //This returns all Readings - in a real world scenario may be better to allow from/to queries
    router.HandleFunc("/bloods", getReadings).Methods("GET")
    
    //Pattern to delete a single reading
    router.HandleFunc("/blood/{id}", deleteReading).Methods("DELETE")
    
    //Pattern to create a reading with id
    router.HandleFunc("/blood/{id}", updateReadingPUT).Methods("PUT")
    
    
   //These stubs are here to remind me what needs to be created and to allow compilation as we go
   func GetReadings(w http.ResponseWriter, r *http.Request) {}
   func getReading(w http.ResponseWriter, r *http.Request) {}
   func updateReadingPUT(w http.ResponseWriter, r *http.Request) {}
   func CreateReadingPOST(w http.ResponseWriter, r *http.Request) {}
   func deleteReading(w http.ResponseWriter, r *http.Request) {}
```

At this point we have defined the routes for our multiplexer but not created the webserver to listen for these requests.  This is achieved with Go's standard `http.ListenAndServe` function
```go
func ListenAndServe(addr string, handler Handler) error

```

Fortunately the `mux.Router` implements the interface `Handler` so this can just be passed to `ListenAndServe`

```go 
//starts the server and blocks
log.Fatal(http.ListenAndServe(":9001", router))

```

Note that `ListenAndServer` blocks and so will only return when it errors and therefore we can call `log.Fatal` on the returning error to see what went wrong, such as trying to bind an already bound port
```
2019/02/02 10:40:09 listen tcp :9001: bind: address already in use

```

The Server has been hard coded to port 9001.  In a later section we will look at passing a bind address as either a parameter or in an environment variable

## Writing the Functions to Support the Service

### deleteReading
The simplest function to write is the `deleteReading(http.ResponseWriter, *http.Request)` method. The returns no body and either a 200 (`http.StatusOK`) for success or 404 `http.StatusNotFound` when a reading with the given ID is not found

The other thing that is needed is to bisect the URI to determine the ID of the reading to be deleted.  One of the reasons for using the mux package is that it does the work and the reading can just be queried.  First the parameters hashmap needs to be requested from the HTTP Request and parsed by the multiplexer `params := mux.Vars(r)`.  The ID is now as easy to read as `params["id"]`.

Now that the requested ID is obtained it is as simple as calling `blood.RemoveReadingByID(params["id"])` and the result then tested for success or failure.

On success the 200 return is written into the ResponseWriter and on failue a 404 is sent back to the client

```go
func deleteReading(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    b:=blood.RemoveReadingByID(params["id"])
    if b {
        w.WriteHeader(http.StatusOK)
    } else {
        w.WriteHeader(http.StatusNotFound)
    }
}
```

The local variable is not actually required but there is little point in removing it as the compiler will optimise it out along with the params local variable 

### getReadings

This function is designed to return the JSON representation of all readings held in the set.

First the function needs to retrieve the JSON encoding from the blood package `blood.ToJSON()`  When an error is returned we need to send an error condition back to the caller with a `http.StatusInternalServerError` as this should never occur.  On success we write back the JSON with an appropriate header:

```go
func getReadings(w http.ResponseWriter, r *http.Request) {
    output,error:=blood.ToJSON()
    if error!=nil {
        log.Print(error.Error())
        w.WriteHeader(http.StatusInternalServerError)
        e:=`{"error": "`+error.Error()+`"}`+`\n`
        io.WriteString(w,e)
        return  
    }
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    w.Write(output)
    io.WriteString(w,"\n")
}

```

Note that once the WriteHeader has been called the header cannot be changed and so setting all other header variables must be completed before this.  Additionally, writing to the ResponseWriter also closes the header.

The function uses both w.Write and io.WriteString to show different approaches only.

### Creating a New Reading

The previous two functions cannot be tested because we have no data and at the moment no way of adding data.  This sub-section will look at two ways that data can be created

- Using POST on `/bloods`
- Using PUT on `/blood/{id}` where the reading with `{id}` doesn't exist

Because there are two methods the core code will be extracted out into its own function to be called by the PUT and POST routines.  However, during writing this it became obvious that the library is  missing code for the error condition when the new reading to be added has an ID of another reading and also matches the date/time of a third reading.  Adding this to the `BGSet.go` defined in the blood pkg:

```go
    if date_found && id_found && (pbg_date!=pbg_id) { //different dated record already exists as well as a record with this id       
        return fmt.Errorf("Readings already exists at %s with the ID %s and at %s with ID %s",pbg_date.Date.Format(time.RFC3339),pbg_date.ID, pbg_id.Date.Format(time.RFC3339),pbg_id.ID)
    }

```

> > *Don't forget to `go build` the library after the change*

Try and add the reading and capture any error produced  
```go
func createReading(reading blood.BGReading, w http.ResponseWriter) {
    error := blood.AddReading(reading)
```

When an error occurs produce a log message to the stdout and log the error then return StatusNotAcceptable because the record isn't appropriate and send JSON value of the error

```go
    if error!=nil {
        log.Printf("creating reading with id and date %s, %s",reading.ID,reading.Date.Format(time.RFC3339))
        log.Print(error.Error())
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        e:=`{"error": "`+error.Error()+`"}`+"\n"
        io.WriteString(w,e)
        
    } else {
```

The reading was added so send success and retrieve the stored value in case the act of storing changes any value.  Not the program assumes that nothing will have changed during these calls but this may not be the case in a multithreaded system.  A later section will look at making out system thread safe.

```go       
        w.Header().Set("Content-Type", "application/json") 
        w.WriteHeader(http.StatusCreated)
        storedReading,_:=blood.FindReadingByID(reading.ID) // it was put without error so should be ok- in multithreaded process this wouldn't be true
        bs,err:=storedReading.ToJSON()
        if err==nil {
           w.Write(bs)
           io.WriteString(w,"\n")
        }
    }
}
```

#### Create via POST

The first thing to do is decode the JSON passed in the body to a BGReading.  If it will not decode then we need to return and error to the caller.  

```go
    var reading blood.BGReading
    err := json.NewDecoder(r.Body).Decode(&reading)

```

When it is successful we call the `createReading` function defined above.  The full function is therefore defined as 

```go
func createReadingPOST(w http.ResponseWriter, r *http.Request) {
    var reading blood.BGReading
    err := json.NewDecoder(r.Body).Decode(&reading)
    if err!=nil {
        log.Printf("The body is not a BGReading! Error is %s. Body is %s",err.Error(), r.Body)
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        
        e:=`{"error": "`+err.Error()+`"}`+"\n"
        io.WriteString(w,e)
        return
    }
    createReading(reading,w)
    
}


```

#### Create via PUT

To create via the PUT method the client has had to pass in an ID that didn't exist in the set.  To split the code into a create or update is a simple test of the ID passed in the URI

```go
func updateReadingPUT(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    _,b:=blood.FindReadingByID(params["id"])
    if b { //update could handle but will update non matching id if dates match which I want to avoid
        updateReading(w,r)
    } else {
        createReadingPUT(w,r) 
    }
}

```

Note that we do not inspect the ID in the body so this could have a different value to that in the URL in which case a Reading with a different value will be created to that in the URL.  The function could decode and correct the body or throw an error if it is needed to.  Note that the Update part changes the decoded body.  Note however that the update function in the library can do the create but will also update a matching reading with a different ID if the dates match.  

### Update Reading via HTTP PUT

This is made via PUT which makes a call to `updateReadingPUT` as defined above which forwards on to `updateReading`.  This function decodes the body into a BGReading and returns an error to the client if it fails. 

```go
func updateReading(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    log.Printf("params=%v",params)
    var reading blood.BGReading
    var origID string 
    err  := json.NewDecoder(r.Body).Decode(&reading)
    if err!=nil {

        log.Printf("The body is not a BGReading! Error is %s. Body is %s",err.Error(), r.Body)
        w.WriteHeader(http.StatusNotAcceptable)
        w.Header().Set("Content-Type", "application/json")
        e:=`{"error": "`+err.Error()+`"}`+"\n"
        io.WriteString(w,e)
        return
    }
```

On successful decoding the id parameter and the reading id and check for equality and the reading changed if they are not.
```go
    if reading.ID != params["id"] {
        log.Printf("ID in url and body not the same, overriding the body: %s %s.  Setting reading to URL ID",params["id"],reading.ID)
        origID=reading.ID
        reading.ID=params["id"]
    }
```

Now call the library update
```go
    //this should handle create as well as update 
    b := blood.UpdateReadingByID(reading)
```

On failure log and send error back to the client
```go
    if b!=true {
        msg:=fmt.Sprintf("updating reading with id and date %s, %s",reading.ID,reading.Date.Format(time.RFC3339))
        log.Print(msg)
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        e:=`{"error": "`+msg+`"}`+"\n"
        io.WriteString(w,e)
```


On success get the updated reading from the set and return this to the client.  Not that this code will handle the case of updating a non matching id with a matching time.
```go
    } else {
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusAccepted)
        storedReading,bf:=blood.FindReadingByID(reading.ID) 
        if !bf {
            storedReading,bf=blood.FindReadingByID(origID)
        }
        
        bs,err:=storedReading.ToJSON()
        if err==nil {
           w.Write(bs)
           io.WriteString(w,"\n")
        }
    }
}

```

### Get Reading via HTTP GET Method

This is the last of the methods needed and is simple and follows the same pattern as the other functions

```go
func getReading(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    reading,b:=blood.FindReadingByID(params["id"])
    if b { 
        json,err := reading.ToJSON()
        if err!=nil {
            w.WriteHeader(http.StatusInternalServerError)
            return
        }
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusOK)
        w.Write(json)
    } else {
        w.WriteHeader(http.StatusNotFound)
       
    }
}
```

## Changing the IP:Port

To override the default bind address `":9001"` the program shall read the contents of either an environment variable called `BLOODREST_BIND`, which can take a value of `host:port`, or (higher precedence) command line flags `-host/-h` and `-port/-p` that specifies the `ip` to bind to and the `port`.  Note that port must be specified for the host to be used

To read environment variables Go provide an `os` package that allows programs to do this in a OS independent format.  The `os` package has a LookupEnv function that returns the value for the environment variable we are searching and a flag if it was found.  

```go
var bind string
var yes bool

bind,yes = os.LookupEnv("BLOODREST_BIND")
if !yes {
//set default value
            bind=":9001"
}
```

That was easy so onto reading the command line flags. This requires us to define the flags and where they are to be stored when located and then to parse the arguments

```go

var host string
var port int


flag.StringVar(&host,"host","","The binding IP")
flag.StringVar(&host,"h","","The binding IP (shorthand)")
flag.IntVar(&port,"port",0,"Binding port")
flag.IntVar(&port,"p",0,"Binding port (shorthand)")

flag.Parse()
```

The `flag.StringVar` function requires:
- pointer to a string to store the value when found
- the string of the flag to search for.  It automatically appends a `-/--`
- the default value
- description of used wrong for help text

The `flag.IntVar`is defined the same but in this call the default value being set is zero

Next, the program parses (`flag.Parse()`) the command line and populates the variables `host` and `port`

Tp set the precedence required the program will execute this first and only check the environment variable if the port is zero and then populate the bind variable to the final value

```go
var bind,host string
var port int
var yes bool
    
flag.StringVar(&host,"host","","The binding IP")
flag.StringVar(&host,"h","","The binding IP (shorthand)")
flag.IntVar(&port,"port",0,"Binding port")
flag.IntVar(&port,"p",0,"Binding port (shorthand)")
flag.Parse()
    
if port==0 {
    bind,yes = os.LookupEnv("BLOODREST_BIND")
    if !yes {
        bind=":9001"
    }
} else {
    bind=fmt.Sprintf("%s:%v",host,port)
}
```

Finally we use the bind variable in the ListenAndServe call

```go
log.Print("listening on "+bind)
log.Fatal(http.ListenAndServe(bind, router))

```

### Testing the Service

We can test the service easily using curl.

```
> curl -X PUT -H 'Content-Type: application/json' -d '{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}' localhost:9001/blood/id01
{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id01"}
```

Here the curl command is calling the service using the PUT method and passing the JSON representation of a reading.  The id created is that of the URI and not in the data

Add a couple of further readings

```
> curl -X PUT -H 'Content-Type: application/json' -d '{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}' localhost:9001/blood/id02
{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id02"}

> curl -X POST -H 'Content-Type: application/json' -d '{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}' localhost:9001/bloods
{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}

```

The last one is added using the POST.  

Quick test to make sure errors are thrown    
- No data

```
> curl -v -X POST localhost:9001/bloods
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9001 (#0)
> POST /bloods HTTP/1.1
> Host: localhost:9001
> User-Agent: curl/7.63.0
> Accept: */*
> 
< HTTP/1.1 406 Not Acceptable
< Content-Type: application/json
< Date: Sun, 03 Feb 2019 11:37:01 GMT
< Content-Length: 17
< 
{"error": "EOF"}
* Connection #0 to host localhost left intact
```

- empty data

``` 
> curl -v -X POST -d {} localhost:9001/bloods
Note: Unnecessary use of -X or --request, POST is already inferred.
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9001 (#0)
> POST /bloods HTTP/1.1
> Host: localhost:9001
> User-Agent: curl/7.63.0
> Accept: */*
> Content-Length: 2
> Content-Type: application/x-www-form-urlencoded
> 
* upload completely sent off: 2 out of 2 bytes
< HTTP/1.1 406 Not Acceptable
< Content-Type: application/json
< Date: Sun, 03 Feb 2019 11:38:46 GMT
< Content-Length: 51
< 
{"error": "Cannot add a reading with a zero date"}
* Connection #0 to host localhost left intact

``` 

- Some duff data

```
> curl -v -X POST -d '{"Date": 45}' localhost:9001/bloods
Note: Unnecessary use of -X or --request, POST is already inferred.
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9001 (#0)
> POST /bloods HTTP/1.1
> Host: localhost:9001
> User-Agent: curl/7.63.0
> Accept: */*
> Content-Length: 12
> Content-Type: application/x-www-form-urlencoded
> 
* upload completely sent off: 12 out of 12 bytes
< HTTP/1.1 406 Not Acceptable
< Content-Type: application/json
< Date: Sun, 03 Feb 2019 11:41:57 GMT
< Content-Length: 90
< 
{"error": "parsing time "45" as ""2006-01-02T15:04:05Z07:00"": cannot parse "45" as """"}
* Connection #0 to host localhost left intact

```

Checking that we can get all readings

```
> curl http://localhost:9001/bloods
[{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id01"},{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id02"},{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}]

```

Add an already existing reading

```
> curl -X PUT -H 'Content-Type: application/json' -d '{"Reading":6,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}' localhost:9001/blood/id03
{"error": "A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}

```

A few more test cases

```
> curl -X POST -H 'Content-Type: application/json' -d '{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","ID":"id04"}' localhost:9001/bloods
{"error": "A reading already exists at 2019-01-25T08:59:00Z with the ID 1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}

> curl -X POST -H 'Content-Type: application/json' -d '{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 3","ID":"id03"}' localhost:9001/bloods
{"error": "A reading already exists at 2019-01-25T07:59:00Z with the ID id02"}

```

Delete a reading

```
> curl -v -X DELETE http://localhost:9001/blood/id01
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9001 (#0)
> DELETE /blood/id01 HTTP/1.1
> Host: localhost:9001
> User-Agent: curl/7.63.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Sun, 03 Feb 2019 11:48:41 GMT
< Content-Length: 0
< 
* Connection #0 to host localhost left intact

```

Try and get the Reading `id01` just deleted

```
> curl http://localhost:9001/bood/id01
404 page not found
```

## Concurrent Access

The http server started supports providing concurrent access to our REST service.  Therefore the code has to be thread safe in that it has to be able to provide consistent and accurate results each time it is called.  However, the library produced in the first articles that is being used is definitely not thread safe because the Set implementation states it isnt thread safe.


### Goroutines

Go's standard concurrent processes are known as `goroutines`.  To declare a goroutine it is a simple as calling the `go` and passing the function to be run.

A goroutine is started using the go statement

```go
function foo(arguments) {}


...

  go foo(arguments)
``` 

or using an anonymous function

```go
go func() { a = "hello" }()
```

Concurrency in Go can be rather subtle especially around observing changes between go routines and it is recommended that the reader look at the documentation [here](https://golang.org/ref/mem)

The code supporting the REST Service does not need to create new goroutines as the `ListenAndServe` function creates a new goroutine for each incoming connection.  From the documentation:

>Serve accepts incoming HTTP connections on the listener l, creating a new service **goroutine** for each. The service goroutines read requests and then call handler to reply to them. 

It is therefore important to realise that all functions need to be thread safe 

#### Communication Between Goroutines 

Go provides communication channels to allow one goroutine to send data to another goroutine.  One of the standard patterns to recieve information back from a goroutine is to pass the channel into the goroutine as an argument to the function call.

Channels are defined as single value pipes and request from the channel block until a value is available to read and writes to a channel blocks until there is a receiver ready to receive.  Note that the later can be overridden if a channel is defined with a certain size in which case it will block when it is full

Channels are typed and is therefore defined like any other variable.

```go
var c chan int  

``` 

This defines variable c to hold a channel to send and int between its sender and receiver.  Not that this declaration is nil as a channel hasn't yet been created.

The easiest way is to use make `make(chan int)` or with a size `make(chan int, 100)`

Channels can also be defined so that they have an explicit direction, ie read from or written to.  This is normally used in a function call

```go
 <-chan int \\read channel for ints
 chan<- *Data \\ write channel passing a pointer to a Data Struct
```

If a pointer to a structure or value is being passed around be careful that it is only ever modified by the goroutine that has the pointer otherwise any program may incur a race condition of some form as the Go program will not guarantee when any update occurs unless there is some form os synchronisation.

#### Blocking 

##### Channels

Blocking can be achieved by use of channels in that a goroutine will wait until it can read data before it will continue.  To aid this we can use the `select` statement that will listen for a message on multiple channels 

```go
select {
case i1 = <-c1:
	print("received ", i1, " from c1\n")
case c2 <- i2:
	print("sent ", i2, " to c2\n")
case i3, ok := (<-c3):  // same as: i3, ok := <-c3
	if ok {
		print("received ", i3, " from c3\n")
	} else {
		print("c3 is closed\n")
	}
case a[f()] = <-c4:
	// same as:
	// case t := <-c4
	//	a[f()] = t
default:
	print("no communication\n")
}
``` 


Without a `default` section the select would block forever

This could then be used to protect the non-threadsafe by having a single central process that receives request to read/write to the set and then forward this on appropriately to the correct method.  This is a common pattern used in Go programs.

##### Mutex

However, for the simple case that needs solving there is another approach that is simpler and appropriate in this case.  

The `Mutex` type is found in the `sync` package and has  `(*Mutex)Lock()` and `(*Mutex)Unlock()` methods.  When a Lock is attempted the call blocks until it is free to receive the lock.  

To make this as safe as possible the program shall block on all reads and writes and where it reads after writing we unlock after this.  The simplest option would be to Lock at the start of each routine and then have a deferred unlock which would therefore block like a synchronised method in java.  However, this may not be the most efficient if we want high throughput. 

### Update the Service to Handle Concurrent Calls


Define the Mutex

```go
package main

import (
    "encoding/json"
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood"
    "io"
    "time"
    "fmt"
    "os"
    "flag"
    "sync"
)

var mutex *sync.Mutex=&sync.Mutex{}

func main() {

```

Make use of the mutex

```go
func getReadings(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    
    mutex.Lock()
    output,error:=blood.ToJSON()
    mutex.Unlock()


    if error!=nil {
        log.Print(error.Error())
        w.WriteHeader(http.StatusInternalServerError)
        e:=`{"error": "`+error.Error()+`"}`+`\n`
        io.WriteString(w,e)
        return
        
    }
    w.WriteHeader(http.StatusOK)
    w.Write(output)
    io.WriteString(w,"\n")
}
```

Other function are similar but there are some slightly more complex case where we read after write

```go
func updateReading(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    log.Printf("params=%v",params)
    var reading blood.BGReading
    var origID string 
    err  := json.NewDecoder(r.Body).Decode(&reading)
    if err!=nil {

        log.Printf("The body is not a BGReading! Error is %s. Body is %s",err.Error(), r.Body)
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        e:=`{"error": "`+err.Error()+`"}`+"\n"
        io.WriteString(w,e)
        return
    }
    if reading.ID != params["id"] {
        log.Printf("ID in url and body not the same, overriding the body: %s %s.  Setting reading to URL ID",params["id"],reading.ID)
        origID=reading.ID
        reading.ID=params["id"]
    }
    
    
    
    //this should handle create as well as update 
    mutex.Lock()
    b := blood.UpdateReadingByID(reading)




    if b!=true {
        mutex.Unlock()



        msg:=fmt.Sprintf("updating reading with id and date %s, %s",reading.ID,reading.Date.Format(time.RFC3339))
        log.Print(msg)
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        e:=`{"error": "`+msg+`"}`+"\n"
        io.WriteString(w,e)
    } else {
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusAccepted)
        storedReading,bf:=blood.FindReadingByID(reading.ID) 
        if !bf {
            storedReading,bf=blood.FindReadingByID(origID)
        }


        mutex.Unlock()


        bs,err:=storedReading.ToJSON()
        if err==nil {
           w.Write(bs)
           io.WriteString(w,"\n")
        }
    }
}
```

### Closing Down the Server Cleanly

Up to this point closing down the Service when it is running has been by a brutal kill irrespective if client's have got a connection.  Using goroutines and communication channels we have the function needed to manage a better solution.

However, it does require that the http.Server function has a shutdown method.  Reading the documentation for `http.Server.Shutdown` provides this example code

```go
package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
)

func main() {
	var srv http.Server

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		// We received an interrupt signal, shut down.
		if err := srv.Shutdown(context.Background()); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// Error starting or closing listener:
		log.Printf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
}

```

This defines a channel to signal that connections have been closed.  A goroutine is spawned to listen for an interrupt from the OS (eg ctrl+c from the shell) and the goroutine waits for this to arrive.  When it arrives it calls Shutdown on the http server passing in a `context`.  Note that the background context does not timeout.  To create a context with a timeout use 
```go
ctx, cancel := context.WithTimeout(context.Background(), duration)
defer cancel()   
```

When the server shutdown completes or times out the channel is closed. and the program completes.    

Combining this concept into the program

```go
srv := &http.Server{
        Addr:         bind,
        // Good practice to set timeouts to avoid Slowloris attacks.
        WriteTimeout: time.Second * 15,
        ReadTimeout:  time.Second * 15,
        IdleTimeout:  time.Second * 60,
        Handler: router, // Pass our instance of gorilla/mux in.
    }
    
    idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint
		log.Print("Interrupt Received")
        
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
        defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server Shutdown: %v", err)
		} 
		close(idleConnsClosed)
	}()
    
    log.Print("Blood Rest Server listening on "+bind)
    if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// Error starting or closing listener:
		log.Printf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
    
    log.Print("Blood Rest Server Shutdown")
```

