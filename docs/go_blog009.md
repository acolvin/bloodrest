---
title: Go Blog Part 9 - Publishing the Blogs on Github Pages
author: Andrew Colvin
date: May 2019

---

# Publishing the Blogs on Github Pages

This is a short article to bring you the reader up to date with the go project.  The changes this time is that I have automated the Gitlab CI/CD Pipeline publishing directly to my company Github Pages, branding the blogs appropriately and integrating the Gitlab pipeline with the corporate Github.

## Repository Restructure

As I have reached the end of digits 1-9 I have had to rename the blog articles so that they now start 001 --> 009 allowing me to continue beyond 9. I also took the opportunity to move the articles into the `docs` folder to separate them from the go code with the output `pdf` and `odt` placed into the `docs/out` directory.

## Changes to the Pipeline

Because I moved the articles I have had to change the pipeline

```

pages:
  image: python:alpine
  stage: document
  script:
  - pip install mkdocs
  - pip install mkdocs-windmill
  - pip install mkdocs-material
  - echo $CI_PROJECT_URL
  - cd $CI_PROJECT_DIR
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
    refs:
      - master
    changes: 
      - "docs/*blog*.md"
      - "docs/README.md"
      - "docs/**"
      - "mkdocs.yml"
      - "dxc-mkdocs.yml"
      - ".gitlab-ci.yml"



```

This restructure means that I no longer have to copy the files into the doc folder in the docker image and make links

```
 	
diff --git a/.gitlab-ci.yml b/.gitlab-ci.yml
index 965a477704bc5788bb4db5f5b986d608af494500..470c379956501d90cb25d7fb257584bfaddbec19 100644
--- a/.gitlab-ci.yml
+++ b/.gitlab-ci.yml
@@ -67,7 +67,7 @@ document:
     artifacts:
       paths:
         - "*.pdf"
-      expire_in: 1 hour
+      expire_in: 1 week
       
 pages:
   image: python:alpine
@@ -76,9 +76,6 @@ pages:
   - pip install mkdocs
   - pip install mkdocs-windmill
   - pip install mkdocs-material
-  - mkdir $CI_PROJECT_DIR/docs
-  - cp *.md $CI_PROJECT_DIR/docs
-  - ln -s $CI_PROJECT_DIR/Pictures $CI_PROJECT_DIR/docs/Pictures
   - cd $CI_PROJECT_DIR
   - mkdocs build
   - mv site public
@@ -87,7 +84,7 @@ pages:
     - public
   only:
      changes: 
-        - "*blog*.md"
-        - "README.md"
+        - "docs/*blog*.md"
+        - "docs/README.md"
         - "mkdocs.yml"

```

There are a few additional cleanups completed.  Please look through the repository to see what they are but an important one is that the site is now deployed in a deploy stage so that the pipeline now consists
![](Pictures/4stagepipeline.png)

## Adding the GitHub Pages Deployment

To deploy to the github I am goint to add a new job in the deploy stage.  This will use `mkdocs` to build and deploy the site directory it builds to the appropriate gh-pages branch on my github repo.

This pipeline job is shown in the code block

```

dxc-GH-Pages:
  image: host.acolvin.me.uk/mkdocs-dxc:latest
  stage: deploy
  script:
  - pip install mkdocs
  - pip install mkdocs-material
  - cd $CI_PROJECT_DIR
  - mv dxc-mkdocs.yml mkdocs.yml
  - mkdocs build
  - mkdocs gh-deploy --clean -r git@github.dxc.com:acolvin/go-blood.git
  artifacts:
    paths:
    - site
  when: manual
  only:
    refs:
      - master
    variables:
      - $CI_PROJECT_URL =~ /.*git.acolvin.me.uk.*/
    changes: 
      - "docs/*blog*.md"
      - "docs/README.md"
      - "docs/**"
      - "dxc-mkdocs.yml"
      - ".gitlab-ci.yml"


```

The job is named `dxc-GH-Pages` which is part of the deploy stage and uses the image called `host.acolvin.me.uk/mkdocs-dxc:latest`.  This image is created so that it is private and contains my private ssh key to connect to my GitHub Repo.  Why have I done this instead of using a variable to pass an api token?  It was to ensure the token was not shown in any logs and thereby expose the repo to outside influence.  

You will have noticed that I have used a new command `mkdocs gh-deploy --clean -r git@github.dxc.com:acolvin/go-blood.git`  This tells `mkdocs` to push the site folder to `origin` on the gh-pages branch.  As this is being executed on my personal gitlab server this would push to the wrong location.  Therefore I have added the `-r` option to specify the remote (in this case my DXC Repository).

Additional controls are that it will only deploy from branch master which is a gitlab protected branch and available to the a restricted group (me in this case) but other devs would need to work on other branches and make pull requests. 

```
  only:
    refs:
      - master
```

Further the job is marked as manual so the pipeline stops and waits for the maintainer to trigger it manually

```
  when: manual


```

Finally it checks that project URL contains `git.acolvin.me.uk` otherwise it will not schedule the job.  This is there to restrict it to my private repo and not execute the job when the repo is pushed to the public GitLab site.

There is also an additional dxc flavoured mkdocs.yml that is used to replace the standard one.  This contains the references to brand the site appropriately

```
site_name: Andrew GOes on a Journey
site_author: 'Andrew Colvin'

theme: 
   name: material
   palette:
     primary: 'white'
     accent: 'yellow'
   logo: 'images/DXCSymbol.gif'
   favicon: 'images/dxc-favicon.ico'
   feature:
     tabs: true
markdown_extensions:
  - meta
repo_url: https://github.dxc.com/acolvin/go-blood
repo_name: 'Go Journey'


```

### Docker Image

The docker image that is used to build the mkdocs site for this job is specialised for the task.  Its definition is shown below

```
FROM python:alpine
RUN apk add --no-cache git git-lfs
RUN apk add git-fast-import openssh
RUN pip install mkdocs
RUN pip install mkdocs-material

RUN mkdir -p /home
RUN adduser  --home /home/andrew  --disabled-password andrew
RUN mkdir /home/andrew/.ssh
COPY id_rsa.pub /home/andrew/.ssh/
COPY id_rsa /home/andrew/.ssh/
COPY known_hosts /home/andrew/.ssh/
RUN chmod 700 /home/andrew/.ssh/
RUN chmod 600 /home/andrew/.ssh/id_rsa

RUN chown -R andrew /home/andrew
WORKDIR /home/andrew

USER andrew



```

It is based on the pythone image built with the alpine base.  I install git, git-lfs, mkdocs and openssh as they are not part of this image.  I then create the user andrew with the directory structure required and copy my ssh private and public key  and set up the permissions appropriately.  Finally before I switch to this user and set the working directory to the users home.

This is then built and pushed to my repository

```
docker build -t host.acolvin.me.uk/mkdocs-dxc:latest . -f python_dxckey.dockerfile
docker push host.acolvin.me.uk/mkdocs-dxc:latest

```

>  As an aside my local repository uses a certificate signed my internal CA as you would expect.  To ensure that docker accepts this certificate I have added  my CA certificate into the folder `/etc/docker/certs.d/host.acolvin.me.uk` on the gitlab server and all clients that connect my internal docker repository.
>  

## Output log

The following block shows the output log from this build job

```
  
Running with gitlab-runner 11.8.0 (4745a6f3)
  on shared SRMXYYg5
Using Docker executor with image host.acolvin.me.uk/mkdocs-dxc:latest ...
Pulling docker image host.acolvin.me.uk/mkdocs-dxc:latest ...
Using docker image sha256:a8442c80d058265020d27908af4bd7d9f8469544ba6225c15d43ab9df00762fb for host.acolvin.me.uk/mkdocs-dxc:latest ...
Running on runner-SRMXYYg5-project-1-concurrent-0 via f4ab565f4efe...
Fetching changes...
Removing public/
HEAD is now at 6f4d458 try to restrict dxc pages to my gitlab
Checking out 6f4d4582 as master...
Skipping Git submodules setup
Downloading artifacts for pages (279)...
Downloading artifacts from coordinator... ok        id=279 responseStatus=200 OK token=SnFroN8J
$ pip install mkdocs
Requirement already satisfied: mkdocs in /usr/local/lib/python3.7/site-packages (1.0.4)
Requirement already satisfied: tornado>=5.0 in /usr/local/lib/python3.7/site-packages (from mkdocs) (6.0.2)
Requirement already satisfied: livereload>=2.5.1 in /usr/local/lib/python3.7/site-packages (from mkdocs) (2.6.0)
Requirement already satisfied: Jinja2>=2.7.1 in /usr/local/lib/python3.7/site-packages (from mkdocs) (2.10.1)
Requirement already satisfied: click>=3.3 in /usr/local/lib/python3.7/site-packages (from mkdocs) (7.0)
Requirement already satisfied: PyYAML>=3.10 in /usr/local/lib/python3.7/site-packages (from mkdocs) (5.1)
Requirement already satisfied: Markdown>=2.3.1 in /usr/local/lib/python3.7/site-packages (from mkdocs) (3.1)
Requirement already satisfied: six in /usr/local/lib/python3.7/site-packages (from livereload>=2.5.1->mkdocs) (1.12.0)
Requirement already satisfied: MarkupSafe>=0.23 in /usr/local/lib/python3.7/site-packages (from Jinja2>=2.7.1->mkdocs) (1.1.1)
Requirement already satisfied: setuptools>=36 in /usr/local/lib/python3.7/site-packages (from Markdown>=2.3.1->mkdocs) (41.0.1)
WARNING: You are using pip version 19.1, however version 19.1.1 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.
$ pip install mkdocs-material
Requirement already satisfied: mkdocs-material in /usr/local/lib/python3.7/site-packages (4.2.0)
Requirement already satisfied: Pygments>=2.2 in /usr/local/lib/python3.7/site-packages (from mkdocs-material) (2.3.1)
Requirement already satisfied: pymdown-extensions>=4.11 in /usr/local/lib/python3.7/site-packages (from mkdocs-material) (6.0)
Requirement already satisfied: mkdocs>=1 in /usr/local/lib/python3.7/site-packages (from mkdocs-material) (1.0.4)
Requirement already satisfied: Markdown>=3.0.1 in /usr/local/lib/python3.7/site-packages (from pymdown-extensions>=4.11->mkdocs-material) (3.1)
Requirement already satisfied: livereload>=2.5.1 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (2.6.0)
Requirement already satisfied: click>=3.3 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (7.0)
Requirement already satisfied: tornado>=5.0 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (6.0.2)
Requirement already satisfied: PyYAML>=3.10 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (5.1)
Requirement already satisfied: Jinja2>=2.7.1 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (2.10.1)
Requirement already satisfied: setuptools>=36 in /usr/local/lib/python3.7/site-packages (from Markdown>=3.0.1->pymdown-extensions>=4.11->mkdocs-material) (41.0.1)
Requirement already satisfied: six in /usr/local/lib/python3.7/site-packages (from livereload>=2.5.1->mkdocs>=1->mkdocs-material) (1.12.0)
Requirement already satisfied: MarkupSafe>=0.23 in /usr/local/lib/python3.7/site-packages (from Jinja2>=2.7.1->mkdocs>=1->mkdocs-material) (1.1.1)
WARNING: You are using pip version 19.1, however version 19.1.1 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.
$ cd $CI_PROJECT_DIR
$ mv dxc-mkdocs.yml mkdocs.yml
$ mkdocs build
INFO    -  Cleaning site directory 
INFO    -  Building documentation to directory: /builds/go/bloodglucose/blood/site 
$ mkdocs gh-deploy --clean -r git@github.dxc.com:acolvin/go-blood.git
INFO    -  Cleaning site directory 
INFO    -  Building documentation to directory: /builds/go/bloodglucose/blood/site 
INFO    -  Copying '/builds/go/bloodglucose/blood/site' to 'gh-pages' branch and pushing to GitHub. 
INFO    -  Your documentation should be available shortly. 
Uploading artifacts...
site: found 111 matching files                     
Uploading artifacts to coordinator... ok            id=280 responseStatus=201 Created token=xDK-UfRF
Job succeeded


```

As you can see there are no credentials or api tokens in the log and as long as I keep my docker image secure (which is only available internally on my server) my ssh key is safe.

## DXC Branding

Here you can see the image of the DXC like branding create with mkdocs and the material theme and published pages site 

![](Pictures/dxcbrand.png)

For those in DXC you are probably reading the blog through this site so can feel it and compare against the gitlab deployed pages.

## Conclusion

The hardest part of this process was the integration between Gitlab and Github and to ensure there is no credential leak even in log files.  The image was built outside of the pipeline to control keys.  I doesn't stop a malicious attacker that has got access to the repo and the protected master branch from changing the pipeline job to print echo out the private key but there are always there isn't much I can do about this but secure my accounts on my servers like always.   