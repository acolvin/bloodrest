---

title: Go Blog Part 5 - Jenkins and Go
author: Andrew Colvin
date: February 2019

---

# Jenkins Pipeline for Go

This is a short blog to look at a jenkins pipeline that will test and build our go code.  I have produced this as I made a quick comment about it during the Makefile and docker discussion in the previous article and I got the challenge to help someone who was struggling with just such a pipeline.

I decided in this article to not bother with building in docker but show how to build on a jenkins node which has the go environment set up. 

I thought this would be simple and take about 10 mins and decided to tackle it before my gym session before work but you can guess that I never made it onto the treadmill but did make it to work on time.  Because I ran into a pitful I decided it would be worthwhile writing this and not just push out a jenkinsfile

## Installing Go

In this example I am running jenkins on the same server that Go is installed so for me there is nothing to do.  However, if this isnt the case and you are building on an empty agen there is a Go Plugin that installs the version of the go tools for you (https://wiki.jenkins.io/display/JENKINS/Go+Plugin)

## Getting Started

I created a new Pipeline project in Jenkins called funnily enough go-blood just to match the series and because I am not imaginative and then went straight to configure

![](Pictures/configure1.png)

The first part of the configuration was connecting the pipeline to my Gitlab server and setting pipeline triggers

I decided create a simple pipeline script in jenkins (well I expected it to be half a dozen lines) and then I can just copy it out as a jenkinsfile later

## Building a Script to Go test ./...

I initially created a simple single stage script to run get the code and run `go test ./...`.

```jenkinsfile
pipeline {
    agent(any) 
    
    stages {
        stage('test') {
            
            steps {
               git 'https://gitlab.acolvin.me.uk/go/bloodglucose/blood.git'
               sh label: '', returnStdout: true, script: 'go get ./...'
               sh label: '', returnStdout: true, script: 'go test ./...'
            }
        }
    }
}

```

That is all that is needed - surely...  Running the pipeline will fail as the code is outside the GOPATH (remember back to the first blog).  

This caused me to scratch my head and go lets just set the GOPATH to my pipeline.  To achieve this I added

```jenkinsfile
    environment {
        GOPATH = "${WORKSPACE}"
        
    }
```

This didn't work either! The code was at the root of the GOPATH.  We know from the first article we need the src, pkg and bin directories and the code needs to be in the correct place.  

Clearly I need a setup stage to create the directories required and clone the code into the correct location.  I therefore created a step before test that initialises as required

```jenkinsfile
    stages {
        stage('setup') {
            steps {
               sh label: '', returnStdout: true, script: 'mkdir -p $GOPATH/pkg'
               sh label: '', returnStdout: true, script: 'mkdir -p $GOPATH/bin'
               sh label: '', returnStdout: true, script: 'mkdir -p $GOPATH/src/gitlab.acolvin.me.uk/go/bloodglucose/blood'
               dir("${GOPATH}/src/gitlab.acolvin.me.uk/go/bloodglucose/blood"){
                   git 'https://gitlab.acolvin.me.uk/go/bloodglucose/blood.git'
               }
            }
        }
        stage('test') {
            
            steps {
               
               sh label: '', returnStdout: true, script: 'go get ./...'
               sh label: '', returnStdout: true, script: 'go test ./...'
            }
        }

```

Other than create the directories I also set the working directory to the path required for the code to be exported and then us the git command.  The workspace now looks correct and the pipeline passes

## Building

At this time we have run `go test` but not actually built any output.  Now it is time to add the build stage.  Remember in the previous part a Makefile was developed that builds for multiple platforms.  So lets build using make.

I had to fix the make file as I set an absolute path for the binary so this was changed so that the bin location is `$(PWD)/bin` and will therefore work from whichever location it is running from.

The build stage is now simple

```jenkinsfile
        stage('build') {
            steps {
               dir("${GOPATH}/src/gitlab.acolvin.me.uk/go/bloodglucose/blood") {
                  sh label: '', returnStdout: true, script: 'make build-all'
               }
            }
        }

```

Just set the working directory and run `make build-all`


The jenkinsfile can be found in the repo.  Feel free to use it and run a pipeline

![](Pictures/pipelinedbuild.png)

# Conclusion
The key, of which in my haste, I forgot was ensuring the Go environment was created correctly.  Jenkins nodes can build Go projects so use it.  I may have been easier to use the golang docker image to do a build as we did in the previous article but we would not have stabbed ourself in the eye and reinforced the lesson of create the Go Environment Correctly. 

