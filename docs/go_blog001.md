# My December Project 2018

This year my improvement project during my time off work I have decided
to teach myself Go, the programming language not the Chinese game. Go is
an interesting language and you can take your first steps into it by
following the tutorial <https://tour.golang.org/list> However, the only
real way to learn a language and become semi-lingual is to actually
start speaking it.   

![](./Pictures/100000000000049B000002FAC4DA7B4DBCC5F6BA.png)To this end
I decided to take a fairly complex blood glucose management application
I created using java and start to translate this. This is a rather
complex application with an embedded database, graphing capabilities,
internationalised as it needs to work for people travelling, calculates
trends etc. For this exercise I will start with just modelling the
underlying data model required to support one part of the application. 

This program makes full use of the java OO model as well as events
between the data layer and the user interface although it is a simple
single “user” threaded application, note that Swing (the GUI framework)
uses its own thread so this complicates interactions slightly. 

This article will show how to get started with Go installing, linking it
into my git repositories and writing the first simple data structures
and test cases.

## Installing Go and Setting up the Project Area

For me this was rather easy on my openSUSE linux box; I typed *sudo
zypper in go *and the package manager did everything for me. There is an
MSI installer for windows found on the golang.org site and similar
packages for mac

Go has a rather rigidly defined directory structure required for the go
tools to find the libraries, your source, modules etc and then to build
and install your code. Luckily this structure is rather simple and is
easily created with these commands (on windows you will need to :

```
mkdir -p \~/go/src 

mkdir -p \~/go/bin

```
The src directory is the location of each of your programs that you are
going to write. The bin directory is where your binaries are installed
(windows default location is *%USERPROFILE%\\go).*



The first thing to do is create the project folder with the command
`mkdir \~/go/src/gitlab.acolvin.me.uk/go/bloodglucose` and then first
package to implement the model for blood glucose readings:  
`mkdir -p \~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood`



>
Note that gitlab.acolvin.me.uk is my personal gitlab server and I have
created public group go and subgroup bloodglucose and the project
blood. Creating the project in this structure allows me to use this as a
library in other projects and they will go to my repository and clone
the repo with a simple `go get` command if it is imported. We will see
imports later in the blog. If you are interested in the functionality of
the application and its history then have a look at this [forum topic](https://www.diabetes.co.uk/forum/threads/writing-bg-management-app.56644/)



## Converting The First Class

Let’s have a look at the java code for the first Class we will look at
converting. It is a fairly simple data object class with some setters
and getters but with an internal canonical storage form and the getter
returns the result in the units defined in the object; it also allows
the value to be returned in units requested by the caller. There are
some functions to convert between units and to calculate the HbA1c from
a mmol value and a calculation to determine the number of minutes after
eating

As we have created our package called blood the first thing to do is
create a file in this directory with the package name

Create the file .../blood/BGReading.go 

Each file needs to be added to a package which provides access scope 

<table>
<tbody>
<tr class="odd">
<td>package blood</td>
</tr>
</tbody>
</table>

The Go language is not object oriented so conversion isnt as straight
forward as it would first seem. We need to define a structure to hold
our data 

<table>
<tbody>
<tr class="odd">
<td><p>type BGReading struct {</p>
<p>  Reading float64 //always in mmols</p>
<p>  Date time.Time </p>
<p>  Comment string</p>
<p>}</p></td>
</tr>
</tbody>
</table>

You will have noticed from this segment that go structures its
declarations differently to most languages in that you name the object
and then define its type This is similar with functions in theat the
return type is defined at the end of the function spec. Interesting
functions in go can return any number of returns. The // is the start of
a comment. 

I have used the time package but have not imported it; under the package
declaration add the import statement

<table>
<tbody>
<tr class="odd">
<td><p>import (</p>
<p>    "time"</p>
<p>)</p></td>
</tr>
</tbody>
</table>

We also need a couple of constants. Add the following constant
definitions 

<table>
<tbody>
<tr class="odd">
<td><p>const (</p>
<p>    MMOL=0</p>
<p>    MGDL=1 </p>
<p>)</p></td>
</tr>
</tbody>
</table>

Because I have defined the type and its fields with capital letters they
are all accessible outside this package (exported). If I had used lower
case letters then they would be private to the package

The first functions I am going to create are those required to convert
from one unit to another. These are fixed and are a simple mathematical
calculation.

<table>
<tbody>
<tr class="odd">
<td><p>func ConvertMMOL2MGDL(mmol float64) float64 {</p>
<p>    return mmol*18.016</p>
<p>}</p>
<p></p>
<p>func ConvertMGDL2MMOL(mgdl float64) float64 {</p>
<p>    return mgdl/18.016</p>
<p>}</p></td>
</tr>
</tbody>
</table>

One of the things you will have noticed is that the type of a variable
is specified after the name. Again these functions are exported for use
externally.

Now we need to create our “setter”. Let us start by setting the reading
and converting into its canonical storage format based on the units. The
function definition is 

<table>
<tbody>
<tr class="odd">
<td>func (bg BGReading) SetBGReading(unit int, reading float64) float64 {</td>
</tr>
</tbody>
</table>

This needs a little explanation. The function name is *SetBGReading *and
is exported. It takes an integer unit (expected, but not constrained, to
be either MMOL or MGDL as defined in the constants). The value of the
reading as a float and returns a float. The interesting part in this
declaration is (bg BGReading); this constrains the function to be called
on a BGReading type. 

The full code for this function is now shown with the appropriate
conversion 

<table>
<tbody>
<tr class="odd">
<td><p>func (bg BGReading) SetBGReading(unit int, reading float64) float64 {</p>
<p>    if unit==MMOL {</p>
<p>        bg.Reading=reading</p>
<p>    } else {</p>
<p>        bg.Reading=ConvertMGDL2MMOL(reading)</p>
<p>    }</p>
<p>    return reading;</p>
<p>}</p></td>
</tr>
</tbody>
</table>

Lets build this package using the go command. On our command line we
change our directory into this one

`cd \~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood`

and then run 

`go build`

So our package builds ok

It is always good practice to write unit tests and go has a built in
unit test process. To start we create a file called BGReading\_test.go
(this name is important and must match. Define the test to be in the
blood package and define our test data

<table>
<tbody>
<tr class="odd">
<td><p>package blood</p>
<p>import "testing"</p>
<p>//import "time" //used later</p>
<p>//import "fmt" //used later</p>
<p>type bgcase struct {</p>
<p>    mmol, mgdl float64</p>
<p>}</p>
<p>var cases = []bgcase {</p>
<p>      {0.0,0.0},</p>
<p>      {3.3, 60.0},</p>
<p>      {5.0,90.0},</p>
<p>  }</p></td>
</tr>
</tbody>
</table>

We will start by testing our conversion routines

<table>
<tbody>
<tr class="odd">
<td><p>func TestConvertMMOL(t *testing.T) {</p>
<p>  for _,c :=range cases {</p>
<p>      got := ConvertMMOL2MGDL(c.mmol)</p>
<p>      diff := got - c.mgdl</p>
<p>      if diff &lt; 0 {</p>
<p>          diff *=-1</p>
<p>      }</p>
<p>      if diff &gt; 1.0 {</p>
<p>          t.Errorf("ConvertMMOL2MGDL(%f) == %f, want %f", c.mmol, got, c.mgdl)</p>
<p>      }</p>
<p>  }</p>
<p>}</p></td>
</tr>
<tr class="even">
<td><p>func TestConvertMG(t *testing.T) {</p>
<p>  for _,c :=range cases {</p>
<p>      got := ConvertMGDL2MMOL(c.mgdl)</p>
<p>      diff := got - c.mmol</p>
<p>      if diff &lt; 0 {</p>
<p>          diff *=-1</p>
<p>      }</p>
<p>      if diff &gt; 0.1 {</p>
<p>          t.Errorf("ConvertMGDL2MMOL(%f) == %f, want %f", c.mgdl, got, c.mmol)</p>
<p>      }</p>
<p>  }</p>
<p>}</p></td>
</tr>
</tbody>
</table>

The range iterates over the array of cases returning the index and the
value. Note that Go functions can return any number of values and not
just one as in many languages. As we are not interested in the index
value the for loop has an “\_” in its declaration meaning don’t assign
it to a variable. The variable c gets defined to be of type bgcase, due
to the use of “:=” and not just equals, and set to each array element in
turn. Now we run the conversion on the mmol (mgdl value in the second
one) and save the result. Because we are playing with floats the results
are not going to be exact so the functions calculate the absolute
difference between what we got from the function and the corresponding
value in the array. Finally the difference is checked to be within
acceptable limits for each function.

To run the test we use the command 

\> go test

PASS  
ok      bloodglucose/blood     0.001s

Now we will define a test for our SetReading function

<table>
<tbody>
<tr class="odd">
<td><p>func TestSetReading(t *testing.T) {</p>
<p>    var bg1 BGReading = BGReading{}</p>
<p>    fmt.printf(“bg1.reading = %f”,bg1.SetBGReading(MMOL,5.0))</p>
<p>    var bg2 BGReading = BGReading{}</p>
<p>    fmt.printf(“bg1.reading = %f”,bg2.SetBGReading(MGDL,90.0))</p>
<p>    if bg2.Reading != bg1.Reading {</p>
<p>        t.Errorf("setting failed %f!=%f",bg1.Reading ,bg2.Reading )</p>
<p>    }</p>
<p>}</p></td>
</tr>
</tbody>
</table>

Running the test now (don’t forget to uncomment import “fmt”) outputs 

bg1.reading = 5.000000    
bg2.reading = 4.995560    
PASS  
ok      bloodglucose/blood     0.001s  

Hold on a second there seems to be something wrong… 5.000000 \!=
4.995560 and therefore the test should have failed but it didn’t.  A
little reading later and it seems that Go passes everything by value to
functions and therefore bg1 and bg2 that are passed to the functions are
actually value equivalent copies and not the original structs. To gain
access to the original bg1 and bg2 structs we need to pass the pointers
into the functions. 

To make this change we have to edit our source function to take a
pointer (the \*)

<table>
<tbody>
<tr class="odd">
<td><p>func (bg *BGReading) SetBGReading(unit int, reading float64) float64 {</p>
<p>    if unit==MMOL {</p>
<p>        bg.Reading=reading</p>
<p>    } else {</p>
<p>        bg.Reading=ConvertMGDL2MMOL(reading)</p>
<p>    }</p>
<p>    return bg.Reading;</p>
<p>}</p></td>
</tr>
</tbody>
</table>

Running the test with go build and then go test

bg1.reading = 5.000000    
bg2.reading = 4.995560    
\--- FAIL: TestSetReading (0.00s)  
   BGReading\_test.go:52: setting failed 5.000000\!=4.995560  
FAIL  
exit status 1  
FAIL    bloodglucose/blood     0.001s

Changing our test function to accept a variance of 0.1 or less will
provide a success

<table>
<tbody>
<tr class="odd">
<td><p>func TestSetReading(t *testing.T) {</p>
<p>    var bg1 BGReading = BGReading{}</p>
<p>    fmt.Printf("bg1.reading = %f \n",bg1.SetBGReading(MMOL,5.0))</p>
<p>    var bg2 BGReading = BGReading{}</p>
<p>    fmt.Printf("bg2.reading = %f \n",bg2.SetBGReading(MGDL,90.0))</p>
<p>    diff:=bg2.Reading - bg1.Reading</p>
<p>    if diff &lt; 0 {</p>
<p>        diff=diff*-1.0</p>
<p>    }</p>
<p>    if diff&gt;0.1 {</p>
<p>        t.Errorf("setting failed %f!=%f",bg1.Reading ,bg2.Reading )</p>
<p>    }</p>
<p>}</p></td>
</tr>
</tbody>
</table>

## Setting all values and Allowing for Future Fields

Go does not allow for operator overloading so we cannot define another
SetBGReading function with a different set of parameters but we also do
not want to create a function which has to take all parameters every
time and which would generate a fragile API. Luckily Go has two
capabilities which we can make use of to assist us in making our new
function dynamic. 

The first is variadic functions. We have used a variadic function above
in the form of fmt.Printf. The variadic function allows the caller to
provide a variable number of parameters and then operate over these. A
simple example from go examples is 

<table>
<tbody>
<tr class="odd">
<td><p>func sum(nums ...int) {</p>
<p>        fmt.Print("The sum of ", nums) // Also a variadic function.</p>
<p>        total := 0</p>
<p>        for _, num := range nums {</p>
<p>                total += num</p>
<p>        }</p>
<p>        fmt.Println(" is", total) // Also a variadic function.</p>
<p>}</p></td>
</tr>
</tbody>
</table>

This function takes any number of integers and iterates over this list
and produces the sum of the values. We can therefore make use of this to
pass our values through if only they were all of the same type and we
could determine which parameter is being set. 

The second piece of the puzzle is the way Go treats functions. They are
just values and can be passed around and called dynamically as part of a
function. Putting these two pieces together we are going to use a
variadic function that takes pointers to a function that takes a
\*BGReading. This bewildering function (at least initially) is then
defined as

<table>
<tbody>
<tr class="odd">
<td><p>func (bg *BGReading) SetBGReadingValues(values ...func(*BGReading)) *BGReading {</p>
<p>    for _,value := range values { // iterate over the parameter functions</p>
<p>        value(bg) // call each parameter function on our BGReading</p>
<p>    }</p>
<p>    return bg</p>
<p>}</p></td>
</tr>
</tbody>
</table>

All that is needed now is a setter for each parameter, The simplest one
to define is the Comment setter. Importantly it has to return our
function definition and not actually execute the function as that is
executed by the SetBGReadingValues function

<table>
<tbody>
<tr class="odd">
<td><p>func SetComment(comment string) func(*BGReading) {</p>
<p>    return func(bg *BGReading) {</p>
<p>        bg.Comment=comment</p>
<p>    }</p>
<p>}</p></td>
</tr>
</tbody>
</table>

The function takes our comment string and returns a function taling a
pointer to a BGReading as required. All this function is doing is
returning a function which simply sets the value of the comment to the
string passed in. We define the other parameter methods similarly 

<table>
<tbody>
<tr class="odd">
<td><p>func setBGReadingTime(bg *BGReading, unit int, reading float64, date time.Time, comment string) *BGReading { // internal setter function</p>
<p>    if unit==MMOL {</p>
<p>        bg.Reading=reading</p>
<p>    } else {</p>
<p>        bg.Reading=ConvertMGDL2MMOL(reading)</p>
<p>    }</p>
<p>    if date.IsZero() { // if a zero date is set then set it to the now</p>
<p>        bg.Date=time.Now()</p>
<p>    } else {</p>
<p>        bg.Date=date</p>
<p>    }</p>
<p>    bg.Comment=comment //set the comment</p>
<p>    return bg  </p>
<p>}</p>
<p>func SetDate(date time.Time) func(*BGReading) {</p>
<p>    return func(bg *BGReading) {</p>
<p>        setBGReadingTime(bg, MMOL, bg.Reading, date, bg.Comment)</p>
<p>    }</p>
<p>}</p>
<p>func SetComment(comment string) func(*BGReading) {</p>
<p>    return func(bg *BGReading) {</p>
<p> //      setBGReadingTime(bg, MMOL, bg.Reading, bg.Date, comment)</p>
<p>        bg.Comment=comment</p>
<p>    }</p>
<p>}</p>
<p>func SetReading(unit int, reading float64) func(*BGReading) {</p>
<p>    return func(bg *BGReading) {</p>
<p>        setBGReadingTime(bg, unit, reading, bg.Date, bg.Comment)</p>
<p>    }</p>
<p>}</p></td>
</tr>
</tbody>
</table>

Now that we have all of the setters defined it is time to write a test
function to check our variadic method

<table>
<tbody>
<tr class="odd">
<td><p>func TestBGValues(t *testing.T) {</p>
<p>    var bg1 BGReading = BGReading{}</p>
<p>    var date time.Time = time.Now()</p>
<p>    bg1.SetBGReadingValues(SetDate(date),SetComment("test"))</p>
<p>    bg1.SetBGReadingValues(SetReading(MMOL, 5.0))</p>
<p>    if bg1.Reading!=5.0 || bg1.Comment!="test" || bg1.Date!=date {</p>
<p>        t.Errorf("failed to confirm values for BGReading %v",bg1)</p>
<p>    }</p>
<p>}</p></td>
</tr>
</tbody>
</table>

The values of our BGReading bg1 is set in two parts:

bg1.SetBGReadingValues(SetDate(date),SetComment("test"))

bg1.SetBGReadingValues(SetReading(MMOL, 5.0))

So although not completely transparent in that we cannot just put the
date, comment or reading in the function call. However, using Set…
provides clarity on exactly what is being set.

### An Alternative Approach

A different approach would have been to define each of our setter
functions to follow this pattern

func (bg \*BGReading) SetXXX(parameter1,…) \*BGReading {

Using this pattern we could stream the calls together
bg.SetDate(date).SetComment(“comment”).

## Making our Package Available 

Go has a built in ability to link in directly with source code
repositories. In fact it goes further than this, it is central to its
philosophy and it will get external packages directly from publicly
available repositories. In this section I am going to put my blood
package onto my gitlab service. 

In Gitlab I start by creating a Group to collate all of my Go projects
together (not the best way to group things but it will do for now) and
in this group I create a sub group “bloodglucose”. Both of these groups
are made public

![](./Pictures/10000000000003AF0000028F225DBCECC9BCFC88.png)In this
latter group I create a new project called blood. The name of this needs
to match our package (by Go convention). This project is now available
at the URL <https://gitlab.acolvin.me.uk/go/bloodglucose/blood.git> Now
we have our empty repository Gitlab gives us loads of hints of how to
add our code into the newly created project. 

I am going to do this from the command line. The first this to do is
initialise our code folder as a git repo and then add our GitLab repo as
an origin. Note that although the project is public it is not world
writable

<table>
<tbody>
<tr class="odd">
<td><p>cd ~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood</p>
<p>git init</p>
<p><strong>git</strong> remote add origin https://andrew@<strong>git</strong>lab.acolvin.me.uk/go/bloodglucose/blood.git<br />
</p></td>
</tr>
</tbody>
</table>

The next step is to add our source files to the source control and
commit the changes

<table>
<tbody>
<tr class="odd">
<td><strong>git</strong> add .<br />
<strong>git</strong> commit -m "Initial commit"<br />
<strong>git</strong> push -u origin master<br />
</td>
</tr>
</tbody>
</table>

The files are now safely published to our git repository and the package
is now available for use. To test that Go can find and retrieve our
package we will use the “go get” command

Going to the root of our src and looking at the directories shows that
we do not have a gitlab folder

<table>
<tbody>
<tr class="odd">
<td><p>&gt; cd ~/go/src</p>
<p>&gt; rm -rf gitlab.acolvin.me.uk # for demo purpose only</p>
<p>&gt; ls</p>
<p><strong>blood_glucose</strong>  <strong>github.com</strong>  <strong>golang.org</strong>  <strong>sortedarray</strong><br />
</p>
<p>&gt; go get gitlab.acolvin.me.uk/go/bloodglucose/blood<br />
</p>
<p>&gt; ls</p>
<p><strong>blood_glucose</strong>  <strong>github.com</strong>  <strong>gitlab.acolvin.me.uk</strong>  <strong>golang.org</strong>  <strong>sortedarray</strong><br />
</p>
<p>&gt; cd gitlab.acolvin.me.uk/go/bloodglucose/<br />
&gt; ls</p>
<p><strong>blood</strong><br />
&gt; ls -l blood</p>
<p>total 8<br />
drwxr-xr-x 1 andrew users   66 Dec 12 06:57 <strong>.</strong><br />
drwxr-xr-x 1 andrew users   10 Dec 12 06:57 <strong>..</strong><br />
-rw-r--r-- 1 andrew users 1557 Dec 12 06:57 BGReading.go<br />
-rw-r--r-- 1 andrew users 2205 Dec 12 06:57 BGReading_test.go<br />
drwxr-xr-x 1 andrew users  138 Dec 12 06:57 <strong>.git</strong><br />
</p></td>
</tr>
</tbody>
</table>

Go has used its built in git functions to fetch the code from the
specified repository and has created the folder structure as defined by
the Git Repository. 

You do not actually have to use go get to fetch packages. Go will do
this for you normally just by the simple act of importing the package.
The import statement now however would become

<table>
<tbody>
<tr class="odd">
<td>import “gitlab.acolvin.me.uk/go/bloodglucose/blood”</td>
</tr>
</tbody>
</table>

## Initialisation with Default Values

Go does not provide a mechanism to initialise a Type on creation so a
different approach has to be undertaken. One approach is to make the
type internal to the package but define an interface that is exported.
We would then get a concrete object by use of a factory type method. An
important point about interfaces in Go is that they are not implemented
exactly and are almost a pattern. Anything that implements that pattern
is deemed to implement the interface. 

This use of the interface is therefore a poor way of implementing this
approach. It is much better to just use a factory method without the
interface. 

The following code shows a simple implementation using an Interface

<table>
<tbody>
<tr class="odd">
<td><p>type BGReadingI interface {</p>
<p>    initBG() </p>
<p>}</p>
<p>func NewBGReading() BGReadingI {</p>
<p>    bg:=&amp;BGReading{postPrandialMinutes: -1, initialised: true, Comment: "initialised by New"}</p>
<p>    return bg</p>
<p>}</p>
<p>func (BGReading) initBG() { }</p></td>
</tr>
</tbody>
</table>

And then the test would be 

<table>
<tbody>
<tr class="odd">
<td><p>func TestBRReadingInterface(t *testing.T) {</p>
<p>    var bg BGReadingI = NewBGReading()</p>
<p>    fmt.Printf("interface test %v, type %T\n",bg,bg)</p>
<p>}</p></td>
</tr>
</tbody>
</table>

This little snippet introduces a couple of extra variables in the struct
postPrandialMinutes which needs to be defaulted to -1 and initialised
which is set to true once it has been initialised (this is
implementation variable and internal only). 

So if we are not going to use the interface approach then we change our
code to

<table>
<tbody>
<tr class="odd">
<td><p>type BGReading struct {</p>
<p>  Reading float64 //always in mmols</p>
<p>  Date time.Time </p>
<p>  Comment string</p>
<p>  Food string</p>
<p>  Exercise string</p>
<p>  postPrandialMinutes int</p>
<p>  initialised bool</p>
<p>}</p>
<p>func NewBGReading() *BGReading {</p>
<p>    bg:=&amp;BGReading{postPrandialMinutes: -1, initialised: true, Comment: "initialised by New"}</p>
<p>    return bg</p>
<p>}</p></td>
</tr>
</tbody>
</table>

I now have the issue that we may mistakenly forget to use our factory.
How do we guard against this. Well in this simple case we do not need
to. The reason I have the initialise boolean (which has a default zero
value of false) is that I can initialise it on first use. We are
therefore going to change our 2 set functions we have defined and define
an init function

<table>
<tbody>
<tr class="odd">
<td><p>func (bg *BGReading) init() { </p>
<p>    if !bg.initialised {</p>
<p>       bg.initialised=true</p>
<p>       bg.postPrandialMinutes=-1</p>
<p>    }</p>
<p>}</p>
<p>func (bg *BGReading) SetBGReading(unit int, reading float64) float64 {</p>
<p>    if unit==MMOL {</p>
<p>        bg.Reading=reading</p>
<p>    } else {</p>
<p>        bg.Reading=ConvertMGDL2MMOL(reading)</p>
<p>    }</p>
<p>    bg.init()</p>
<p>    return bg.Reading;</p>
<p>}</p>
<p>func (bg *BGReading) SetBGReadingValues(values ...func(*BGReading)) *BGReading {</p>
<p>    bg.init()</p>
<p>    for _,value := range values {</p>
<p>        value(bg)</p>
<p>    }   </p>
<p>    return bg</p>
<p>}</p></td>
</tr>
</tbody>
</table>

We are also going to implement the preprandial function as seen in the
java code above

<table>
<tbody>
<tr class="odd">
<td><p>func (bg *BGReading) SetPrePrandialMinutes(prePrandial time.Time) int{</p>
<p>    if bg.Date.IsZero() || prePrandial.IsZero() {</p>
<p>        bg.postPrandialMinutes=-1</p>
<p>    } else {</p>
<p>        var since time.Duration = bg.Date.Sub(prePrandial)</p>
<p>        var minutes int = int(since.Minutes())</p>
<p>        if minutes&lt;0 { minutes=-1}</p>
<p>        bg.postPrandialMinutes=minutes</p>
<p>    }</p>
<p>    bg.initialised=true</p>
<p>    return bg.postPrandialMinutes</p>
<p>}</p></td>
</tr>
</tbody>
</table>

Because postPrandialMinutes is not capitalised it cannot be accessed
directly so we can either choose to export it by changing the variable
to be capitialised or write a getter. Structurally it is more important
that Set is always called to put a value into this field that we will
define a getter as well.

<table>
<tbody>
<tr class="odd">
<td><p>func (bg *BGReading) GetPostPrandialMinutes() int {</p>
<p>    return bg.postPrandialMinutes</p>
<p>}</p></td>
</tr>
</tbody>
</table>

Finally just to tidy off the setter I am going to make a version that
can be used in our variadic setter above

<table>
<tbody>
<tr class="odd">
<td><p>func SetPostPrandial(prePrandial time.Time) func(*BGReading) {</p>
<p>    return func(bg *BGReading) {</p>
<p>        bg.SetPrePrandialMinutes(prePrandial)</p>
<p>    }</p>
<p>}</p></td>
</tr>
</tbody>
</table>

There are a few additional public fields in the Java code without
setters or getters or any related code so these will be added as is. The
default false values are fine so no need to adjust the init function

<table>
<tbody>
<tr class="odd">
<td><p>type BGReading struct {</p>
<p>  Reading float64 //always in mmols</p>
<p>  Date time.Time </p>
<p>  Comment string</p>
<p>  Food string</p>
<p>  Exercise string</p>
<p>  postPrandialMinutes int</p>
<p>  initialised bool</p>
<p>  Fasting bool</p>
<p>  RealFasting bool</p>
<p>  Preprandial bool</p>
<p>}</p></td>
</tr>
</tbody>
</table>

The last unimplemented parts are equal and compare. These are defined in
the java version for the ordered set that all of the BGReadings are
stored. These are therefore not implemented yet until we work on the
implementation of this.

## Documenting our Code

Go has a documentation feature muck like javadoc and not surprisingly it
is imaginatively name godoc. Godoc reads comments immediately before a
function, variable etc that is exported and produces appropriate output.

## Creating our Set to Manage the Readings

This next section is going to look at creating code in our package which
will manage our set of readings. The requirement for the set is that
they are time unique and can be sorted into a time increasing list. 

Go does not provide a Set interface and in terms of collections it is
not anywhere as rich as modern Java (it reminds me more of java 1.1 in
its diversity of function). Go provides Arrays, Maps and Slices (which
are a subsection of arrays but can be manipulated).

Arrays will guarantee the iteration order but not sort or provide for
uniqueness whereas the Map will provide uniquess of key but does not
guarantee the walk order of the entries.

I therefore need to try and find a Set implementation or write one that
provides a sorted walk of a map.

A quick search brings up a the following code repository on github
<https://github.com/emirpasic/gods/> This project implements many of the
collection frameworks provided by other languages. Reading the
desciptions shows that the treeset is the collection type needed as it
guarantees uniqueness and mantains order based on a comparator
functionality

To start we create a file called BGSet.go and add our package and import
statement 

<table>
<tbody>
<tr class="odd">
<td><p>package blood</p>
<p>import ( </p>
<p>    "github.com/emirpasic/gods/sets/treeset"</p>
<p>    "time" // will be required for time manipulation</p></td>
</tr>
</tbody>
</table>

The set holding the readings needs to be unique and follow a singleton
pattern so anything requesting the BGSet always gets the same set of
readings.

Before the creation of the Treeset it requires a comparison function.
The treeset definition says it needs to accept 2 values of type
interface{} - The empty interface think of it like the java “Object” as
everything implements the empty interface. 

The first action to do is confirm the values sent in are of type
\*BGReading and also create the variable of type \*BGReading from the
emty interfaces. If none of the values cast to type \*BGReading then we
will return a zero value from the comparator function.

<table>
<tbody>
<tr class="odd">
<td><p>func byBGReadingDate(a, b interface{}) int {</p>
<p>   bg1,b1 := a.(*BGReading)</p>
<p>   if !b1 { </p>
<p>       //fmt.Printf("%v is not a *BGReading",a)</p>
<p>       return 0 </p>
<p>   }</p>
<p>   bg2,b2 := b.(*BGReading)</p>
<p>   if !b2 { </p>
<p>       //fmt.Printf("%v is not a *BGReading",b)</p>
<p>       return 0 </p>
<p>   }</p>
<p>   var d1, d2 time.Time</p>
<p>   d1 = bg1.Date.Truncate(time.Minute)</p>
<p>   d2 = bg2.Date.Truncate(time.Minute)</p>
<p>   //fmt.Printf("dates %v,%v\n",d1,d2)</p>
<p>   switch {</p>
<p>       case d1.Before(d2):</p>
<p>           return -1</p>
<p>       case d1.After(d2):</p>
<p>           return 1</p>
<p>       default:</p>
<p>           return 0</p>
<p>   }</p>
<p>}</p></td>
</tr>
</tbody>
</table>

The type assertion line bg1,b1 := a.(\*BGReading) asserts that a is of
type \*BGReading and creates the value of the \*BGReading in variable
bg1 and sets b1 to true. If the assertion fails the value of b1 is false
and bg1 is nil

The times are then set truncated to the minute boundary as BGReadings
are only sensitive to the minute. Finally we use a switch statement to
return either -1, 1 or 0. Note that the go switch statement can use a
function returns and not just integers and strings like Java.

The next step is to define our data structure to hold the readings and
also create the singleton pattern. 

<table>
<tbody>
<tr class="odd">
<td><p>type bgSet struct {</p>
<p>    bgset *treeset.Set</p>
<p>    </p>
<p>}</p>
<p>//instance is a variable used to hold a pointer to a single set (part of the singleton)</p>
<p>var instance *bgSet</p>
<p>var once sync.Once</p>
<p>//getInstance is an internal function to get the one and only instance of bgset</p>
<p>func getInstance() *bgSet {</p>
<p>    once.Do(func() {</p>
<p>        instance = &amp;bgSet{}</p>
<p>        instance.bgset=treeset.NewWith(byBGReadingDate)</p>
<p>    })</p>
<p>    return instance</p>
<p>}</p></td>
</tr>
</tbody>
</table>

The once variable of type sync.Once will ensure that the Do function
will run only once. We will use this to initialise our singleton
instance pattern and return the stored instance which is located as a
package variable. The once.Do method takes a function with no return and
initialises our bgset and the treeset inside it. Note that the code
could have just defined the instance without the bgset structure.

The package needs to control the entries added and removed from the set
as there are calculations that need to run to ensure some of the
calculated values are correct and updated appropriately. This is why
getInstance is local to the package and not provided for external use.

The methods accessible are defined below. Note that any BGReading added
is a copy of any owned by the calling function as we are passing by
value.

<table>
<tbody>
<tr class="odd">
<td><p>//External functions from here</p>
<p>//AddReading operates on a BGReading and adds it to the set of BGReadings. The readings are guaranteed to be date unique and therefore sortable</p>
<p>func AddReading(bg BGReading) {</p>
<p>    i:=getInstance()</p>
<p>    bg.Date=bg.Date.Truncate(time.Minute)</p>
<p>    i.bgset.Add(&amp;bg)</p>
<p>}</p>
<p>//ClearReadings() removes all BGReadings from the set</p>
<p>func ClearReadings() {</p>
<p>    i:=getInstance()</p>
<p>    i.bgset.Clear()</p>
<p>}</p>
<p>//Size() returns the number of elements in the set</p>
<p>func Size() int {</p>
<p>    return getInstance().bgset.Size()</p>
<p>}</p>
<p>//RemoveReadingByDate(time.Time) takes a Time and removes the Reading which has that time.</p>
<p>func RemoveReadingByDate(date time.Time) {</p>
<p>    bg:=NewBGReading()</p>
<p>    bg.SetBGReadingValues(SetDate(date.Truncate(time.Minute)))</p>
<p>    s:=getInstance().bgset</p>
<p>    s.Remove(bg)</p>
<p>}</p>
<p>//FindReadingByDate(time.Time) finds the BGReading that corresponds to this time and returns a pointer to this entry</p>
<p>func FindReadingByDate(date time.Time) (*BGReading,bool) {</p>
<p>    bg:=NewBGReading()</p>
<p>    bg.SetBGReadingValues(SetDate(date.Truncate(time.Minute)))</p>
<p>    set:=getInstance().bgset</p>
<p>    foundIndex, foundValue := set.Find(func(index int, value interface{}) bool {</p>
<p>        t,ok := value.(*BGReading)</p>
<p>        if !ok {</p>
<p>            fmt.Printf("%T is not a BGReading\n", value)</p>
<p>            return false</p>
<p>        }</p>
<p>return atsametime(bg, t)</p>
<p>})</p>
<p>    if foundIndex != -1 {</p>
<p>        r:=foundValue.(*BGReading)</p>
<p>        return r,true</p>
<p>    } else {</p>
<p>        return nil,false</p>
<p>    }</p>
<p>    </p>
<p>}</p></td>
</tr>
</tbody>
</table>

Note that FindReadingByDate returns a pointer to the reading. This
allows for external code to modify our dataset externally to our package
and as such opens us to potential coding issues. This really needs to be
changed to return a copy and not the original reading structure but then
also needs to handle updating values.

The last couple of functions export the set to JSON and import it. This
JSON conversion is generally handled transparently by Go for all of the
primitives and the treeset does the same.

<table>
<tbody>
<tr class="odd">
<td><p>//ToJSON() returns a byte array as the JSON representation of the bgset or an error</p>
<p>func ToJSON() ([]byte, error) {</p>
<p>    s:= getInstance().bgset</p>
<p>    json,err:=s.ToJSON()</p>
<p>    if err != nil {</p>
<p>        fmt.Println("got error", err)</p>
<p>        return *new([]byte),err    </p>
<p>    }</p>
<p>    return json,err</p>
<p>}</p>
<p>func FromJSON(data []byte, add bool) error {</p>
<p>    //convert JSON to array of BGReading</p>
<p>    var readings []BGReading</p>
<p>    err := json.Unmarshal(data, &amp;readings)</p>
<p>    </p>
<p>    //if error return</p>
<p>    if err != nil {</p>
<p>        fmt.Printf("error converting json %v\n",err)</p>
<p>        return err</p>
<p>    } </p>
<p>    //if !add then replace so clear s</p>
<p>    if !add {ClearReadings()}</p>
<p>    //add each BGReading to Set</p>
<p>    for _,r:=range readings {</p>
<p>        AddReading(r)</p>
<p>    }</p>
<p>    //return nil</p>
<p>    return nil</p>
<p>}</p></td>
</tr>
</tbody>
</table>

All these additions require additional imports

<table>
<tbody>
<tr class="odd">
<td><p>import ( </p>
<p>    "github.com/emirpasic/gods/sets/treeset"</p>
<p>    "time"</p>
<p>    "sync"</p>
<p>    "fmt"</p>
<p>    "encoding/json"</p>
<p>)</p></td>
</tr>
</tbody>
</table>

To test this package we create another test file 

<table>
<tbody>
<tr class="odd">
<td>BGSet_test.go</td>
</tr>
<tr class="even">
<td><p>package blood</p>
<p>import "testing"</p>
<p>import "time"</p>
<p>import "fmt"</p>
<p>var bgdataset []*BGReading</p>
<p>func initdata() {</p>
<p>    var start time.Time = time.Date(2018,time.December,1,9,0,0,0,time.UTC)</p>
<p>    period,_ := time.ParseDuration("1h")</p>
<p>    date := start</p>
<p>    bg1 := BGReading{}</p>
<p>    bg1.SetBGReadingValues(SetDate(date),SetComment("test"),SetReading(MMOL, 5.0))</p>
<p>    bgdataset = append(bgdataset,&amp;bg1)</p>
<p>    </p>
<p>    date=date.Add(period)</p>
<p>    bg2 := BGReading{}</p>
<p>    bg2.SetBGReadingValues(SetDate(date),SetComment("test1"),SetReading(MMOL, 7.80))</p>
<p>    bgdataset = append(bgdataset,&amp;bg2)</p>
<p>    </p>
<p>    date=date.Add(period)</p>
<p>    bg3 := BGReading{}</p>
<p>    bg3.SetBGReadingValues(SetDate(date),SetComment("test2"),SetReading(MMOL, 6.80))</p>
<p>    bgdataset = append(bgdataset,&amp;bg3)</p>
<p>    </p>
<p>    date=date.Add(period)</p>
<p>    bg4 := BGReading{}</p>
<p>    bg4.SetBGReadingValues(SetDate(date),SetComment("test3"),SetReading(MMOL, 4.80))</p>
<p>    bgdataset = append(bgdataset,&amp;bg4)</p>
<p>    </p>
<p>    </p>
<p>}</p>
<p>func TestBGSet(t *testing.T) {</p>
<p>    fmt.Println("Testing BGSet")</p>
<p>    initdata()</p>
<p>    </p>
<p>    for _,c :=range bgdataset {</p>
<p>        fmt.Printf("adding %p:%v\n",c,*c)</p>
<p>        AddReading(*c)</p>
<p>    }</p>
<p>    a0,_:=ToJSON()</p>
<p>    fmt.Printf("%s\n",string(a0))</p>
<p>    </p>
<p>    if Size() !=4 { //test all items added</p>
<p>       t.Errorf("set does not contain 4 items %d", Size())</p>
<p>    }</p>
<p>    var date time.Time = time.Date(2018,time.December,1,11,0,0,0,time.UTC)</p>
<p>    RemoveReadingByDate(date)</p>
<p>    a,_:=ToJSON()</p>
<p>    fmt.Printf("%s\n",string(a))</p>
<p>    if Size() !=3 { //test reading was removed </p>
<p>        </p>
<p>        </p>
<p>       t.Errorf("set does not contain 3 items %d\n%v", Size(),string(a))</p>
<p>       </p>
<p>    }</p>
<p>    date = time.Date(2018,time.December,1,10,0,0,0,time.UTC)</p>
<p>    r,b := FindReadingByDate(date)</p>
<p>    if b != true { //check that find works</p>
<p>        </p>
<p>       t.Errorf("didnt find expected record")</p>
<p>       </p>
<p>    } else {</p>
<p>        r.SetBGReadingValues(SetComment("changed after find"))</p>
<p>        aa,_:=ToJSON() //test conversion to JSON</p>
<p>        fmt.Printf("%s\n%s\n",string(a),string(aa))</p>
<p>        err:=FromJSON(aa,false) //test conversion from JSON</p>
<p>        if err!=nil { </p>
<p>            t.Errorf("FromJSON error %v\n",err)</p>
<p>        }</p>
<p>    }</p>
<p>    </p>
<p>}</p></td>
</tr>
<tr class="odd">
<td>BGReading.go</td>
</tr>
<tr class="even">
<td><p>func (bg *BGReading) ToJSON() ([]byte,error) {</p>
<p>    json,err:=json.Marshal(bg)</p>
<p>    return json,err</p>
<p>}</p>
<p>func (bg *BGReading) FromJSON(data []byte) error {</p>
<p>    err:=json.Unmarshal(data,bg)</p>
<p>    if err!=nil {</p>
<p>        return err</p>
<p>    }</p>
<p>    bg.postPrandialMinutes=-1</p>
<p>    bg.setID()</p>
<p>    bg.initialised=true</p>
<p>    return nil</p>
<p>}</p></td>
</tr>
</tbody>
</table>

## Conclusion and What Next

Converting these very simple java data objects to Go has shown to be
more complex that expected. We have tripped over several of the
limitations of the language and had to learn that Go does real pass by
value with deep copy unlike Java which doesn’t. We now have to deal with
pointers and realise that we do not have objects but functions and data
structures which have a loser connection than Java objects

My first impression is that there is a large gulf between the
functionality of Go compared to Java but that is by design. Go aims to
be clean and simple but definitely requires the user of the language to
understand pass by value and pointer and how to make best use of the
concepts. 

However we have also set up our environment and created our build
framework which was very simple and integrated this into git. The build
and test process is as simple as it can get. 

In the next session I will implement a unique ID for each reading and
implement a simple REST interface – although a departure from the
original code which is a desktop application. This will introduce a few
more libraries and also discuss concurrency and also build into a
container for deployment along with a main program that will be
executable.

I hope you have found this interesting and if you are experienced in Go
(remember I am learning here) then do provide comments on how to make
improvements.
