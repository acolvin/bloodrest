package blood

import (
    "github.com/emirpasic/gods/sets/treeset"
    "time"
    "sync"
    "fmt"
    "encoding/json"
    //"log"
    "io"
)


func init() {
    _ = getInstance()
}
// byBGReadingDate is a comparator that returns -1 when bg1 date is earlier than bg2
// and 0 when the dates are the same and 1 when bg1 date is greater than BG2s
func byBGReadingDate(a, b interface{}) int {
   bg1,b1 := a.(*BGReading)
   if !b1 {
       //fmt.Printf("%v is not a *BGReading",a)
       return 0
   }
   bg2,b2 := b.(*BGReading)
   if !b2 {
       //fmt.Printf("%v is not a *BGReading",b)
       return 0
   }
   var d1, d2 time.Time
   d1 = bg1.Date.Truncate(time.Minute)
   d2 = bg2.Date.Truncate(time.Minute)
   //fmt.Printf("dates %v,%v\n",d1,d2)
   switch {
       case d1.Before(d2):
           return -1
       case d1.After(d2):
           return 1
       default:
           return 0
   }
}

func atsametime(a,b *BGReading) bool {
    if a.Date.Truncate(time.Minute).Equal(b.Date.Truncate(time.Minute)) {
        return true
    } else {
        return false
    }
}



//bgSet is an internal structure that is used to hold the set of BGReadings and its associated data
type bgSet struct {
    bgset *treeset.Set

}

//instance is a variable used to hold a pointer to a single set (part of the singleton)
var instance *bgSet
var once sync.Once

//getInstance is an internal function to get the one and only instance of bgset
func getInstance() *bgSet {
    once.Do(func() {
        instance = &bgSet{}
        instance.bgset=treeset.NewWith(byBGReadingDate)
    })
    return instance
}

//External functions from here

//AddReading operates on a BGReading and adds it to the set of BGReadings.  The readings are guaranteed to be date unique and therefore sortable
func AddReading(bg BGReading) error {
    if bg.Date.IsZero() {
        return fmt.Errorf("Cannot add a reading with a zero date")
    }
    if bg.ID=="" {
        return fmt.Errorf("Cannot add a reading without an ID")
    }
    i:=getInstance()
    bg.Date=bg.Date.Truncate(time.Minute)
    
    pbg_date,date_found  := FindReadingByDate(bg.Date)
    pbg_id,id_found := findReadingByID(bg.ID)
    //log.Printf("date_found %b id_found %b", date_found ,id_found)
    if date_found && id_found && (pbg_date==pbg_id) { //record already exists
        return nil
    }
    if date_found && id_found && (pbg_date!=pbg_id) { //different dated record already exists as well as a record with this id       
        return fmt.Errorf("Readings already exists at %s with the ID %s and at %s with ID %s",pbg_date.Date.Format(time.RFC3339),pbg_date.ID, pbg_id.Date.Format(time.RFC3339),pbg_id.ID)
    }
    if date_found && !id_found { //reading with this date already exists
        return fmt.Errorf("A reading already exists at %s with the ID %s",bg.Date.Format(time.RFC3339), pbg_date.ID)
    }
    if !date_found && id_found { //reading with this ID exists at a different date
        return fmt.Errorf("A reading with ID %s exists with a different time %s", bg.ID, pbg_id.Date.Format(time.RFC3339))
    }
    i.bgset.Add(&bg)
    return nil
}

//ClearReadings() removes all BGReadings from the set
func ClearReadings() {
    i:=getInstance()
    i.bgset.Clear()
}

//Size() returns the number of elements in the set
func Size() int {
    return getInstance().bgset.Size()
}

//RemoveReadingByDate(time.Time) takes a Time and removes the Reading which has that time.
func RemoveReadingByDate(date time.Time) {
    bg:=NewBGReading()
    bg.SetBGReadingValues(SetDate(date.Truncate(time.Minute)))
    s:=getInstance().bgset
    s.Remove(bg)
}

//Finds and removes the reading with the ID provided.  It returns false if not found and true otherwise
func RemoveReadingByID(id string) bool {
    bg,b:=findReadingByID(id)
    if !b {
        return false
    }
    s:=getInstance().bgset
    s.Remove(bg)
    return true
}

//FindReadingByDate(time.Time) finds the BGReading that corresponds to this time and returns a pointer to this entry
func FindReadingByDate(date time.Time) (*BGReading,bool) {
    bg:=NewBGReading()
    bg.SetBGReadingValues(SetDate(date.Truncate(time.Minute)))
    set:=getInstance().bgset
    foundIndex, foundValue := set.Find(func(index int, value interface{}) bool {
        t,ok := value.(*BGReading)
        if !ok {
            fmt.Printf("%T is not a BGReading\n", value)
            return false
        }
		return atsametime(bg, t)
	})
    if foundIndex != -1 {

        //arr:=set.Values()
        //r0:=arr[foundIndex]
        r:=foundValue.(*BGReading)
        //rcopy:=*r
        //r.SetBGReadingValues(SetComment("changed after find in function"))
        //fmt.Printf("\n\ncopy of found is %v\nfoundvalue is %v\n changed value %v\n\n\n",rcopy,foundValue,r)
        return r,true
    } else {
        return nil,false
    }

}

func findReadingByID(id string) (*BGReading, bool) {
    set:=getInstance().bgset
    foundIndex, foundValue := set.Find(func(index int, value interface{}) bool {
        t,ok := value.(*BGReading)
        if !ok {
            fmt.Printf("%T is not a BGReading\n", value)
            return false
        }
        return t.ID==id
    })
    if foundIndex != -1 {
        r:=foundValue.(*BGReading)
        return r,true
    } else {
        return nil,false
    }
}

//FindReadingByID locates a BGReading with the given ID and returns a copy of this BGReading and a bool value of true.  If the record is not found a zero BGReading is returned and a false boolean
func FindReadingByID(id string) (BGReading,bool) {
    var reading BGReading
    var preading *BGReading
    var found bool
    preading,found=findReadingByID(id)
    if found {
        reading=*preading
        return reading,true
    } else {
        return *new(BGReading),false
    }
}

// UpdateReadingbyID finds the reading referenced by the ID in the supplied BGReading and applies the exported values to the stored BGReading.  Note that if the time is being changed then the old entry will be removed from the set updated and then reapplied to ensure the correctness of the ordering.  The operation returns true if the reading is found and can be updated with the given values, or there is not an entry with the reading date/time and this isnt a zero time in which case it is inserted into the set otherwise false is returned
func UpdateReadingByID(reading BGReading) bool {
    var currentReading *BGReading
    var found bool
    if reading.ID=="" {
        //fmt.Printf("ID is zero")
        return false
    }
    reading.SetBGReadingValues(SetDate(reading.Date.Truncate(time.Minute)))
    currentReading,found = findReadingByID(reading.ID)

    if !found {
        //record with ID doesn't exist
        if reading.Date.IsZero() {
            //date is zero so cannot add
            return false
        }
        currentReading,found= FindReadingByDate(reading.Date)
        if !found {
            // An existing record with this time doesnt exist so can add this as a new record
            AddReading(reading)
            //get the reading that was added to confirm action
            currentReading,found = findReadingByID(reading.ID)
            if !found {
                return false
            } else {
                return true
            }
        }
    }

    //check if the date is changing and remove and readd to set if it is
    if !reading.Date.Equal(currentReading.Date) {
        //check there isnt already a dated entry
        _,oops:=FindReadingByDate(reading.Date)
        if oops {
            return false
        }
        getInstance().bgset.Remove(currentReading)
        currentReading.Date=reading.Date
        getInstance().bgset.Add(currentReading)
    }

    //Just update values
    currentReading.Reading=reading.Reading
    currentReading.Comment=reading.Comment
    currentReading.Food=reading.Food
    currentReading.Exercise=reading.Exercise
    currentReading.Fasting=reading.Fasting
    currentReading.RealFasting=reading.RealFasting
    currentReading.Preprandial=reading.Preprandial


    return true
}

//ToJSON() returns a byte array as the JSON representation of the bgset or an error
func ToJSON() ([]byte, error) {
    s:= getInstance().bgset
    //fmt.Println(s)
    json,err:=s.ToJSON()
    if err != nil {
        fmt.Println("got error", err)
        return *new([]byte),err

    }
    //fmt.Println(string(json))
    return json,err
}

func FromJSON(data []byte, add bool) error {
    //s:= getInstance().bgset
    //convert JSON to array of BGReading
    var readings  []BGReading
    err := json.Unmarshal(data, &readings)

    //if error return
    if err != nil {
        fmt.Printf("error converting json %v\n",err)
        return err
    } //else {
        //fmt.Printf("json array is %v\n",readings)
    //}
    //if !add then replace so clear s
    if !add {ClearReadings()}
    //add each BGReading to Set
    //var warn = make([]int, len(readings), len(readings))
    for _,r:=range readings {
        AddReading(r)
    }
    //return nil
    return nil
}

func (*bgSet) String() string {
    res:=""
    it:=getInstance().bgset.Iterator()
    for it.Next() {
        res=fmt.Sprintf("%s\n%v",res,it.Value())
    }
    return res
}

func StreamJSON(w io.Writer) {

    it:=getInstance().bgset.Iterator()
    for it.Next() {
        v:=it.Value().(*BGReading)
        vv, err :=v.ToJSON()
        if err==nil {
            w.Write(vv)
            w.Write([]byte("\n"))
        }
    }
    //w.Write([]byte("\n"))
}
