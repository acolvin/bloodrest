package blood

import (
    "time"
    "encoding/json"
//    "github.com/sony/sonyflake"
    "github.com/segmentio/ksuid"

//    "fmt"
)

const (
    MMOL=0
    MGDL=1

)



type BGReading struct {
  Reading float64 //always in mmols
  Date time.Time
  Comment string
  Food string
  Exercise string
  postPrandialMinutes int
  initialised bool
  Fasting bool
  RealFasting bool
  Preprandial bool
  ID string
}

func NewBGReading() *BGReading {
    bg:=&BGReading{postPrandialMinutes: -1, initialised: true, Comment: "initialised by New"}

    bg.ID=ksuid.New().String()

    return bg
}

func (bg *BGReading) init() {
    if !bg.initialised {
       bg.initialised=true
       bg.postPrandialMinutes=-1
       bg.setID()
    }
}

func (bg *BGReading) setID() {
   if bg.ID=="" {
       bg.ID=ksuid.New().String()
   }
}

func (bg *BGReading) SetBGReading(unit int, reading float64) float64 {
    if unit==MMOL {
        bg.Reading=reading
    } else {
        bg.Reading=ConvertMGDL2MMOL(reading)
    }
    bg.init()
    return bg.Reading;
}

func ConvertMMOL2MGDL(mmol float64) float64 {
    return mmol*18.016
}

func ConvertMGDL2MMOL(mgdl float64) float64 {
    return mgdl/18.016
}

func ConvertMMOLtoHbA1c(mmol float64) float64 {// mmol/l to mmol/mol estimation
   //AGmmol/l=1.59 × A1CNGSP − 2.59
   //IFCC = 10.93 NGSP - 23.50
   return 10.93*(mmol+2.59)/1.59-23.50
}

func ConvertHbA1cIFCCtoNGSP(ifcc float64) float64 {
    return (ifcc+23.50)/10.93
}

func ConvertHbA1cNGSPtoIFCC(ngsp float64) float64 {
    return 10.93*ngsp-23.50
}

func setBGReadingTime(bg *BGReading, unit int, reading float64, date time.Time, comment string) *BGReading {
    if unit==MMOL {
        bg.Reading=reading
    } else {
        bg.Reading=ConvertMGDL2MMOL(reading)
    }
    if date.IsZero() {
        bg.Date=time.Now().Truncate(time.Minute)//needs to be truncated to minute and set to UTC
    } else {
        bg.Date=date.Truncate(time.Minute) //needs to be truncated to minute and set to UTC
    }
    bg.Comment=comment
    return bg
}

func SetDate(date time.Time) func(*BGReading) {
    return func(bg *BGReading) {
        setBGReadingTime(bg, MMOL, bg.Reading, date, bg.Comment)
    }
}

func SetComment(comment string) func(*BGReading) {
    return func(bg *BGReading) {
        bg.Comment=comment
    }
}

func SetFood(food string) func(*BGReading) {
    return func(bg *BGReading) {
        bg.Food=food
    }
}

func SetExercise(exercise string) func(*BGReading) {
    return func(bg *BGReading) {
        bg.Exercise=exercise
    }
}

func SetReading(unit int, reading float64) func(*BGReading) {
    return func(bg *BGReading) {
        setBGReadingTime(bg, unit, reading, bg.Date, bg.Comment)
    }
}

func (bg *BGReading) SetBGReadingValues(values ...func(*BGReading)) *BGReading {
    bg.init()
    for _,value := range values {
        value(bg)
    }
    return bg
}

func (bg *BGReading) SetPrePrandialMinutes(prePrandial time.Time) int{
    if bg.Date.IsZero() || prePrandial.IsZero() {
        bg.postPrandialMinutes=-1
    } else {
        var since time.Duration = bg.Date.Sub(prePrandial)
        var minutes int = int(since.Minutes())
        if minutes<0 { minutes=-1}
        bg.postPrandialMinutes=minutes
    }
    bg.initialised=true
    bg.setID()
    return bg.postPrandialMinutes
}

func SetPostPrandial(prePrandial time.Time) func(*BGReading) {
    return func(bg *BGReading) {
        bg.SetPrePrandialMinutes(prePrandial)
    }
}

func (bg *BGReading) GetPostPrandialMinutes() int {
    return bg.postPrandialMinutes
}

func (bg *BGReading) ToJSON() ([]byte,error) {
    json,err:=json.Marshal(bg)
    return json,err
}

func (bg *BGReading) FromJSON(data []byte) error {
    err:=json.Unmarshal(data,bg)
    if err!=nil {
        return err
    }
    bg.Date=bg.Date.Truncate(time.Minute)
    bg.postPrandialMinutes=-1
    bg.setID()
    bg.initialised=true
    return nil
}
