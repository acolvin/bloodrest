package blood

import "testing"
import "time"
//import "fmt"


type bgcase struct {
    mmol, mgdl float64
}

var cases = []bgcase {
      {0.0,0.0},
      {3.3, 60.0},
      {5.0,90.0},
  }


func TestConvertMMOL(t *testing.T) {
  for _,c :=range cases {
      got := ConvertMMOL2MGDL(c.mmol)
      diff := got - c.mgdl
      if diff < 0 {
          diff *=-1
      }
      if diff > 1.0 {
          t.Errorf("ConvertMMOL2MGDL(%f) == %f, want %f", c.mmol, got, c.mgdl)
      }
  }
}

func TestConvertMG(t *testing.T) {
  for _,c :=range cases {
      got := ConvertMGDL2MMOL(c.mgdl)
      diff := got - c.mmol
      if diff < 0 {
          diff *=-1
      }
      if diff > 0.1 {
          t.Errorf("ConvertMGDL2MMOL(%f) == %f, want %f", c.mgdl, got, c.mmol)
      }
  }
}

func TestSetReading(t *testing.T) {
    var bg1 BGReading = BGReading{}
    t.Logf("bg1.reading = %f \n",bg1.SetBGReading(MMOL,5.0))
    //t.Logf("Reading is set to %f on struct %p\n", bg1.Reading, bg1)
    var bg2 BGReading = BGReading{}
    t.Logf("bg2.reading = %f \n",bg2.SetBGReading(MGDL,90.0))
    //t.Logf("Reading is set to %f on struct %p\n", bg2.Reading, bg2)
    diff:=bg2.Reading - bg1.Reading
    if diff < 0 {
        diff=diff*-1.0
    }
    if diff>0.1 {
        t.Errorf("setting failed %f!=%f",bg1.Reading ,bg2.Reading )
    }
}

//func TestSetReadingandDate(t *testing.T) {
//    d:=time.Now()
//    var bg1 *BGReading = &BGReading{}
//    bg1.SetBGReadingTime(MMOL,5.0,d,"test" )
//    t.Logf("Reading is set to %f on struct %p\n", bg1.Reading, bg1)
//    var bg2 *BGReading = &BGReading{}
//    bg2.SetBGReadingTime(MGDL,90.0,d,"test" )
//    t.Logf("Reading is set to %f on struct %p\n", bg2.Reading, bg2)
//    if bg2.Reading != bg1.Reading {
//        t.Errorf("setting failed %f!=%f",bg1.Reading ,bg2.Reading )
//    }
//}

func TestBGValues(t *testing.T) {
    var bg1 BGReading = BGReading{}
    var date time.Time = time.Now()
    bg1.SetBGReadingValues(SetDate(date),SetComment("test"))
    bg1.SetBGReadingValues(SetReading(MMOL, 5.0))
    if bg1.Reading!=5.0 || bg1.Comment!="test" || bg1.Date!=date.Truncate(time.Minute) {
        t.Errorf("failed to confirm values for BGReading %v",bg1)
    }
    json,err:=bg1.ToJSON()
    if err!=nil {
        t.Errorf("BGReading ToJSON failed %v\n",err)
    } else {
        t.Logf("JSON Value = %s\n",string(json))
        var bg2 BGReading = BGReading{}
        err = (&bg2).FromJSON(json)
        if err!=nil {
            t.Errorf("BGReading FromJSON failed %v\n",err)
        } else {
            t.Logf("Original Value = %v\n",bg1)
            t.Logf("FromJSON Value = %v\n",bg2)
        }
    }

}

func TestBRReadingInterface(t *testing.T) {
    var bg *BGReading = NewBGReading()
    t.Logf("interface test %v, type %T\n",bg,bg)
}
