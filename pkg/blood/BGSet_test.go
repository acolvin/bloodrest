package blood

import "testing"
import "time"
//import "fmt"
import "os"
import "bufio"

var bgdataset []*BGReading

func initdata() {
    bgdataset=make([]*BGReading,0)
    var start time.Time = time.Date(2018,time.December,1,9,0,0,0,time.UTC)
    period,_ := time.ParseDuration("1h")
    date := start
    bg1 := BGReading{}
    bg1.SetBGReadingValues(SetDate(date),SetComment("test"),SetReading(MMOL, 5.0))
    bgdataset = append(bgdataset,&bg1)
    
    date=date.Add(period)
    bg2 := BGReading{}
    bg2.SetBGReadingValues(SetDate(date),SetComment("test1"),SetReading(MMOL, 7.80))
    bgdataset = append(bgdataset,&bg2)
    
    date=date.Add(period)
    bg3 := BGReading{}
    bg3.SetBGReadingValues(SetDate(date),SetComment("test2"),SetReading(MMOL, 6.80))
    bgdataset = append(bgdataset,&bg3)
    
    date=date.Add(period)
    bg4 := BGReading{}
    bg4.SetBGReadingValues(SetDate(date),SetComment("test3"),SetReading(MMOL, 4.80))
    bgdataset = append(bgdataset,&bg4)
    
    
}

func TestBGSet(t *testing.T) {
    t.Log("Testing BGSet")
    initdata()
    
    for _,c :=range bgdataset {
        t.Logf("adding %p:%v\n",c,*c)
        AddReading(*c)
    }
    a0,_:=ToJSON()
    t.Logf("%s\n",string(a0))
    
    if Size() !=4 {
       t.Errorf("set does not contain 4 items %d", Size())
    }
    w := bufio.NewWriter(os.Stdout)
    StreamJSON(w)
    w.Flush()
    
    
    var date time.Time = time.Date(2018,time.December,1,11,0,0,0,time.UTC)
    RemoveReadingByDate(date)
    a,_:=ToJSON()
    t.Logf("%s\n",string(a))
    if Size() !=3 {
        
        
       t.Errorf("set does not contain 3 items %d\n%v", Size(),string(a))
       
    }
    date = time.Date(2018,time.December,1,10,0,0,0,time.UTC)
    r,b := FindReadingByDate(date)
    if b != true {
        
       t.Errorf("didnt find expected record")
       
    } else {
        r.SetBGReadingValues(SetComment("changed after find"))
        aa,_:=ToJSON()
        t.Logf("%s\n%s\n",string(a),string(aa))
        err:=FromJSON(aa,false)
        if err!=nil { 
            t.Errorf("FromJSON error %v\n",err)
        }
    }
    
}

func TestAddID(t *testing.T) {
    initdata()
    ClearReadings() //reset Set
    t.Log("")
    t.Log("---testing Add by with IDs---")
    t.Log("")
    for _,c :=range bgdataset {
        e:=AddReading(*c)
        if e!=nil {
            t.Errorf("Adding initial record set fails: %s", e.Error())
            return
        }
    }
    
    t.Log("------Check Add record again------")
    bg:=*bgdataset[1]
    e := AddReading(bg)
    if e!=nil {
        t.Errorf("Adding duplicate record failed: %s", e.Error())
        return
    }
    
    t.Log("------Check Add record New ID existing time------")
    bg=*bgdataset[1]
    bg.ID=""
    bg.setID()
    e = AddReading(bg)
    if e==nil { //should not accept it
        t.Errorf("Adding duplicate time record failed as it didn't throw error")
        return
    } else {
        t.Logf("--------- error returned correctly %s\n", e.Error())
    }
    t.Log("------Check Add record Existing ID different time------")
    bg=*bgdataset[2]
    bg.Date=time.Now().Truncate(time.Minute)    
    e = AddReading(bg)
    if e==nil { //should not accept it
        t.Errorf("Adding duplicate ID record failed as it didn't throw error")
        return
    } else {
        t.Logf("--------- error returned correctly %s\n", e.Error())
    }
    
    t.Log("------Check Add record zero time------")
    bg.Date=*new(time.Time)
    e = AddReading(bg)
    if e==nil { //should not accept it
        t.Errorf("Added reading with zero date didnt throw error %s", bg.Date.Format(time.RFC3339))
        return
    } else {
        t.Logf("--------- error returned correctly %s\n", e.Error())
    }
    t.Log("------Check Add record no ID------")
    bg=*bgdataset[2]
    bg.ID=""
    e = AddReading(bg)
    if e==nil { //should not accept it
        t.Errorf("Added reading with no ID didnt throw error ")
        return
    } else {
        t.Logf("--------- error returned correctly %s\n", e.Error())
    }
}

func TestRemoveReadingByID(t *testing.T) {
    t.Log("")
    t.Log("testing Remove with IDs")
    
    //initialise the data sets
    initdata()
    ClearReadings() //reset Set
    
    for _,c :=range bgdataset {
        e:=AddReading(*c)
        if e!=nil {
            t.Errorf("Adding initial record set fails: %s", e.Error())
            return
        }
    }
   
   t.Logf("  delete reading with ID %s", bgdataset[2].ID)
   success:=RemoveReadingByID(bgdataset[2].ID)
   if !success {
       t.Errorf("failed to remove reading using ID %s", bgdataset[2].ID)
   }
   
   t.Logf("  delete reading with empty ID")
   failure:=RemoveReadingByID("")
   if failure {
       t.Errorf("success returned trying to pass an empty ID")
   }
   
   t.Logf("  delete reading with Non-Existent ID")
   failure=RemoveReadingByID("cant exist")
   if failure {
       t.Errorf("success returned trying to pass a random ID")
   }
   
   t.Logf("  delete the rest out of order")
   success = true
   t.Logf("  delete reading with ID %s", bgdataset[3].ID)
   success= success && RemoveReadingByID(bgdataset[3].ID)
   if !success {
       t.Errorf("failed to remove (last) reading using ID %s", bgdataset[3].ID)
   }
   t.Logf("  delete reading with ID %s", bgdataset[0].ID)
   success= success && RemoveReadingByID(bgdataset[0].ID)
   if !success {
       t.Errorf("failed to remove (first) reading using ID %s", bgdataset[0].ID)
   }
   t.Logf("  delete reading with ID %s", bgdataset[1].ID)
   success= success && RemoveReadingByID(bgdataset[1].ID)
   if !success && Size()!=0 {
       t.Errorf("failed to remove (the only) reading using ID %s", bgdataset[1].ID)
   }
}


func TestFindReadingByID(t *testing.T) {
    t.Log("")
    t.Log("testing FindReadingByID")
    
    //initialise the data sets
    initdata()
    ClearReadings() //reset Set
    
    for _,c :=range bgdataset {
        e:=AddReading(*c)
        if e!=nil {
            t.Errorf("Adding initial record set fails: %s", e.Error())
            return
        }
    }
    
    
    bg1,success:=FindReadingByID(bgdataset[2].ID)
    if !success {
        t.Errorf("Could not Find the reading with ID %s",bgdataset[2].ID)
        t.Fail()
    }
    bg2,success:=findReadingByID(bgdataset[2].ID)
    if !success {
        t.Errorf("Could not find the reading with ID %s",bgdataset[2].ID)
        t.Fail()
    }
    if &bg1==bg2 {
        t.Errorf("The pointers are equal %v=%v",&bg1,bg2)
    } else {
        t.Logf("Find has pointer %p", &bg1)
        t.Logf("find has pointer %p", bg2)
    }
    bg3,failure:=FindReadingByID("")
    if failure {
        t.Errorf("Did not receive false and zero Reading with empty ID %v", bg3)
    }
}

func TestUpdateReadingByID(t *testing.T) {
    t.Log("")
    t.Log("testing UpdateReadingByID")
    
    //initialise the data sets
    initdata()
    ClearReadings() //reset Set
    
    for _,c :=range bgdataset {
        e:=AddReading(*c)
        if e!=nil {
            t.Errorf("Adding initial record set fails: %s", e.Error())
            return
        }
    }
    
    //test fail if no ID
    bgcopy:= *bgdataset[2]
    bgcopy.ID=""
    b:=UpdateReadingByID(bgcopy)
    if b {
        t.Logf("bgcopy is %T %v",bgcopy,bgcopy)
        t.Errorf("should have received a false for empty ID, received %T:%v",b,b)
        return
    }
    
    //test change of non-structural fields
    bgcopy= *bgdataset[3]
    t.Logf("Record to be updated %v",bgcopy)
    bgcopy.SetBGReadingValues(SetFood("changed in update test"),SetExercise("Changed in update test"))
    b=UpdateReadingByID(bgcopy)
    if !b {
        t.Errorf("update failed for simple case")
        
    }
    t.Logf("%s",getInstance())
    
    
    //test update of time
    bgcopy= *bgdataset[2]
    t.Logf("Record to be updated %v",bgcopy)
    bgcopy.Date=time.Now().Truncate(time.Minute)
    t.Logf("                  to %v",bgcopy)
    b=UpdateReadingByID(bgcopy)
    if !b {
        t.Errorf("update failed for date change")
        return
    } else {
        t.Logf("%s",getInstance())
        bgcopy= *bgdataset[2]
        bgcopy.ID="new ID Test Change of Date"
        e:=AddReading(bgcopy)
        if e!=nil && Size()!=5 {
            t.Errorf("Failed to re-add original reading with new ID \n   %s", e.Error())
            return
        } else {
            t.Logf("%s",getInstance())
        }
    }
    
    //test change of time to impact another record
    bgcopy= *bgdataset[1]
    t.Logf("Record to be updated %v",bgcopy)
    bgcopy.Date=bgdataset[2].Date
    t.Logf("                  to %v",bgcopy)
    b=UpdateReadingByID(bgcopy)
    if b {
         t.Errorf("Managed to update record to a date of another record")
         return
    } else {
        t.Logf("new state \n%s",getInstance())
    }
    
    //check add if record not found
    bgcopy= *bgdataset[1]
    b=RemoveReadingByID(bgcopy.ID)
    //check it is gone
    if !b {
        t.Errorf("record to test with couldn't be deleted %v",bgcopy)
        t.Logf("%s",getInstance())
        return
    }
    //now update the reading
    b=UpdateReadingByID(bgcopy)
    if !b {
        t.Errorf("record to test with didn't get added %v",bgcopy)
        t.Logf("%s",getInstance())
        return
    }
    t.Logf("New state after add record code branch %s",getInstance())
    
}
