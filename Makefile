﻿BINARY_NAME=bloodrest
BINARY_DIR=$(PWD)/./bin

all:	install build-all

build-all: build-linux build-pi build-windows

dir:
	mkdir -p $(BINARY_DIR)

deps:
	cd ./pkg/blood && \
	go get 
	cd ./cmd/bloodrest && \
	go get 

debug:
	cd ./pkg/blood && \
	go build -gcflags='-N -l'
	cd ./cmd/bloodrest && \
	go build -gcflags='-N -l'

test: deps
	cd ./pkg/blood && \
	go test

build:	test
	cd ./pkg/blood && \
	go build
	cd ./cmd/bloodrest && \
	go build


install: build
	cd ./pkg/blood && \
	go install
	cd ./cmd/bloodrest && \
	go install

clean:
	cd ./pkg/blood && \
	go clean -i
	cd ./cmd/bloodrest && \
	go clean -i && rm -f $(BINARY_DIR)/$(BINARY_NAME)-*64*

build-linux: test dir
	cd ./cmd/bloodrest && \
	GOOS=linux GOARCH=amd64 go build -o $(BINARY_DIR)/$(BINARY_NAME)-amd64


build-pi:	test dir
	cd ./cmd/bloodrest && \
	GOOS=linux  GOARCH=arm64 go build -o $(BINARY_DIR)/$(BINARY_NAME)-arm64

build-windows:	test dir
	cd ./cmd/bloodrest && \
	GOOS=windows  GOARCH=amd64 go build -o $(BINARY_DIR)/$(BINARY_NAME)-win64.exe




.phony:	install test build deps all clean build-pi build-windows build-linux dir debug build-all

