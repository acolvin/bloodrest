FROM golang:alpine as build
RUN apk update && apk add git && rm -rf /var/cache/apk/*

RUN mkdir -p /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
RUN mkdir -p /go/pkg
RUN mkdir -p /go/bin

COPY ./A_Colvin_CA_version_2.crt /usr/local/share/ca-certificates/A_Colvin_CA_version_2.crt
RUN update-ca-certificates


ADD  . /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
WORKDIR /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood



ENV GOPATH /go


RUN go get ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o bloodrest ./cmd/bloodrest/bloodrest.go

From scratch


COPY --from=build /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bloodrest /app/


WORKDIR /app
CMD ["./bloodrest"]
