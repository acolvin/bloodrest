#uncomment one of the from groups


#for stretch
FROM golang:latest

#for alpine
#FROM golang:alpine
#RUN apk update && apk add git && rm -rf /var/cache/apk/*

RUN mkdir -p /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
RUN mkdir -p /go/pkg
RUN mkdir -p /go/bin
RUN adduser --system --home /go --no-create-home --disabled-password appuser
ADD  . /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
WORKDIR /go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood
RUN chown -R appuser /go


#RUN mkdir /usr/local/share/ca-certificates/acolvinCA
COPY ./A_Colvin_CA_version_2.crt /usr/local/share/ca-certificates/A_Colvin_CA_version_2.crt
RUN update-ca-certificates

 
USER appuser
ENV GOPATH /go
RUN ls -l

RUN go get ./...
RUN go build -o bloodrest ./cmd/bloodrest/bloodrest.go
RUN ls -l



CMD ["./bloodrest"]
