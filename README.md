# Blogs of my Go Journey

This repository holds the a series of blogs documenting my journey into learning about and using GO from my perspective - someone who knows many programming languages and has focused on Java for the many years.  It delves off of the Go path into CI and CD, installing various tools and configuring them 

The Blog is structured into parts due to its size and the end of each blog is tagged in the repository so that you can view the state at the point in time and view the changes through the journey from version to version.


All the code for these blogs can be found in the git repository in my (you know that as you are here): 

- [private gitlab server - the main repository](https://git.acolvin.me.uk/go/bloodglucose/blood) 
- [mirror on the public Gitlab Server](https://gitlab.com/acolvin/bloodrest)
- [mirror on my corporate Github account - closed to DXC employees](https://github.dxc.com/acolvin/go-blood)

I have also created static site versions using `mkdocs` which can be located following one of:

- [The gitlab pages deployment on my private gitlab installation](http://go.pages.acolvin.me.uk/bloodglucose/blood/)
- [The public mirror on Gitlab.com Gitlab Pages](https://acolvin.gitlab.io/bloodrest/)
- [The DXC corporate styled Github Pages version - locked to DXC employees](https://github.dxc.com/pages/acolvin/go-blood/)

Please note that if you have followed an earlier posting to a file which doesn't exist that is because I have restructured the repository as I have moved through the series.  Please select the tag for the blog version your require and you will find the document.  From this version the blog articles are now under the docs folder instead of in the root of the repository and the filenames have been changed to allow easier delivery of more than 9 blogs.  Please follow the path and investigate the repo or alternatively I will try and keep the links below correct.  

## [1.](docs/go_blog001.md) The Journey Begins

- Understanding Go and the pitfalls I fell into as I went. 
- Struggles with conversion from class based language to functional language
- Sorting out initial and default values.

## [2.](docs/go_blog002.md) GOing futher...
- Extending the program 
- init functions
- playing with sets 
- Playing with unique IDs
- testing capabilities

## [2+.](docs/go_blog002_1.md) Squashing those pesky bugs
- Debugging

## [3.](docs/go_blog003.md) Depart for a REST
- Building REST
- Envirtonment variables and command line flags
- Goroutines (Multithreading)
- Channels
- Mutual Exclusivity


## [4.](docs/go_blog004.md) Building on both sides of the void
- Make
- Cross compiling
- building into docker

## [5.](docs/go_blog005.md) Jenkins GO and flow through that pipe

- Jenkins Pipelining without docker
- Added Jenkinsfile

## [6.](docs/go_blog006.md) Gitlab follows down the same pipe.

- Installation of your own gitlab server
- Building the pipeline
- Installing runners to drive the pump
- Collecting the outputs from the end of the pipe


## [7.](docs/go_blog007.md) Converting the Blogs 

- Building a static site with mkdocs
- Installing Gitlab pages
- CI/CD for mkdocs and gitlab pages

## [8.](docs/go_blog008.md) Supporting Large Files with Git

- GIT-LFS installation and turning it on
- Tracking and untracking files
- Using file locks
- Removing GIT-LFS 

## [9.](docs/go_blog009.md) Deploying to Github Pages from Gitlab CI/CD

- Limiting to branches
- Mkdocs gh-pages
- Access control and limiting execution

## [10.](./go_blog010.md) JSON Streaming and File IO

- Return to Go
- Write Interface
- Stream the set
- Adding File IO to the Rest Service
- Stream the Set of Readings to a File on Service Closure

