package main

import (
    "encoding/json"
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood"
    "io"
    "bufio"
    "time"
    "fmt"
    "os"
    "flag"
    "sync"
    "context"
    "os/signal"
    "runtime"
)

var mutex *sync.Mutex=&sync.Mutex{}

func main() {
    
    
    var bind,host string
    var port int
    var yes bool
    var filename string
    var home string
    
    
    
    
    flag.StringVar(&host,"host","","The binding IP")
    flag.StringVar(&host,"h","","The binding IP (shorthand)")
    flag.IntVar(&port,"port",0,"Binding port")
    flag.IntVar(&port,"p",0,"Binding port (shorthand)")
    flag.StringVar(&filename,"file","","file backing store: default $HOME/bloodrest.json")
    flag.StringVar(&filename,"f","","file backing store: default $HOME/bloodrest.json (shorthand)")
    flag.Parse()
    
    
    if port==0 {
        bind,yes = os.LookupEnv("BLOODREST_BIND")
        if !yes {
            bind=":9001"
        }
    } else {
        bind=fmt.Sprintf("%s:%v",host,port)
    }
    
    if filename == "" {
        //home,yes = os.UserHomeDir() // use this for go 1.12 instead of the if
        if runtime.GOOS == "windows" {
          home,yes = os.LookupEnv("%userprofile%")
        } else {
          home,yes = os.LookupEnv("HOME")
        }
        
        if yes {
            filename = home+string(os.PathSeparator)+"bloodrest.json"
        } else {
            filename = "bloodrest.json"
        }
        
    }
    
    f, err := os.Open(filename);
    if err !=nil {
       log.Print(err);   
    } else {
        var i int  
        var bg *blood.BGReading 
        scanner := bufio.NewScanner(f)
        for scanner.Scan() {
            bg=blood.NewBGReading()
            line:=[]byte(scanner.Text())
            if len(line)!=0 {
                err:=bg.FromJSON(line)
                if err !=nil && err!=io.EOF { 
                    log.Print(err)
                } else if err == io.EOF {
                } else {
                    i++
                    blood.AddReading(*bg)
                }
            }
            
        }
        f.Close()
        log.Printf("%v messages read and converted",i)
    }
    
    // Create the http router
    router := mux.NewRouter()
    
    
    
    //This url allows query of a single reading
    router.HandleFunc("/blood/{id}", getReading).Methods("GET")
    
        
    //Pattern to create a reading 
    router.HandleFunc("/bloods", createReadingPOST).Methods("POST")
    
    //This returns all Readings - in a real world scenario may be better to allow from/to queries
    router.HandleFunc("/bloods", getReadings).Methods("GET")
    
    //Pattern to delete a single reading
    router.HandleFunc("/blood/{id}", deleteReading).Methods("DELETE")
    
    //Pattern to create a reading with id
    router.HandleFunc("/blood/{id}", updateReadingPUT).Methods("PUT")
    
    srv := &http.Server{
        Addr:         bind,
        // Good practice to set timeouts to avoid Slowloris attacks.
        WriteTimeout: time.Second * 15,
        ReadTimeout:  time.Second * 15,
        IdleTimeout:  time.Second * 60,
        Handler: router, // Pass our instance of gorilla/mux in.
    }
    
    idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint
		log.Print("Interrupt Received")
        
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
        defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server Shutdown: %v", err)
		} 
		close(idleConnsClosed)
	}()
    
    log.Print("Blood Rest Server listening on "+bind)
    if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// Error starting or closing listener:
		log.Printf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
    
    log.Print("Blood Rest Server Shutdown")
    
    log.Print("Streaming Readings to ", filename)
    writeFile(filename)
    
    
    //starts the server and blocks
    //log.Print("listening on "+bind)
    //log.Fatal(http.ListenAndServe(bind, router))
    
    //never runs....
    //log.Print("listening on "+bind)
}

func writeFile(filename string){
    f,err:=os.Create(filename)
    if err==nil {
       defer f.Close()
       blood.StreamJSON(f)
       f.Sync()
    } else {
        log.Fatal(err)
    }
    
}

//These stubs are here to remind me what needs to be created and to allow compilation as we go
//func GetReadings(w http.ResponseWriter, r *http.Request) {}
//func getReading(w http.ResponseWriter, r *http.Request) {}
//func updateReading(w http.ResponseWriter, r *http.Request) {}
//func CreateReading(w http.ResponseWriter, r *http.Request) {}
//func deleteReading(w http.ResponseWriter, r *http.Request) {}


func getReadings(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    
    mutex.Lock()
    output,error:=blood.ToJSON()
    mutex.Unlock()
    if error!=nil {
        log.Print(error.Error())
        w.WriteHeader(http.StatusInternalServerError)
        e:=`{"error": "`+error.Error()+`"}`+`\n`
        io.WriteString(w,e)
        return
        
    }
    w.WriteHeader(http.StatusOK)
    w.Write(output)
    io.WriteString(w,"\n")
}

func createReadingPOST(w http.ResponseWriter, r *http.Request) {
    var reading blood.BGReading
    err := json.NewDecoder(r.Body).Decode(&reading)
    if err!=nil {
        log.Printf("The body is not a BGReading! Error is %s. Body is %s",err.Error(), r.Body)
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        
        e:=`{"error": "`+err.Error()+`"}`+"\n"
        io.WriteString(w,e)
        return
    }
    createReading(reading,w)
    
}

func updateReadingPUT(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    mutex.Lock()
    _,b:=blood.FindReadingByID(params["id"])
    mutex.Unlock()
    if b { //update could handle but will update non matching id if dates match which I want to avoid
        updateReading(w,r)
    } else {
        createReadingPUT(w,r) 
    }
}

func getReading(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    mutex.Lock()
    reading,b:=blood.FindReadingByID(params["id"])
    mutex.Unlock()
    if b { 
        json,err := reading.ToJSON()
        if err!=nil {
            w.WriteHeader(http.StatusInternalServerError)
            return
        }
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusOK)
        w.Write(json)
    } else {
        w.WriteHeader(http.StatusNotFound)
       
    }
}

func deleteReading(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    mutex.Lock()
    b:=blood.RemoveReadingByID(params["id"])
    mutex.Unlock()
    if b {
        w.WriteHeader(http.StatusOK)
    } else {
        w.WriteHeader(http.StatusNotFound)
    }
}

func updateReading(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    log.Printf("params=%v",params)
    var reading blood.BGReading
    var origID string 
    err  := json.NewDecoder(r.Body).Decode(&reading)
    if err!=nil {

        log.Printf("The body is not a BGReading! Error is %s. Body is %s",err.Error(), r.Body)
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        e:=`{"error": "`+err.Error()+`"}`+"\n"
        io.WriteString(w,e)
        return
    }
    if reading.ID != params["id"] {
        log.Printf("ID in url and body not the same, overriding the body: %s %s.  Setting reading to URL ID",params["id"],reading.ID)
        origID=reading.ID
        reading.ID=params["id"]
    }
    //this should handle create as well as update 
    mutex.Lock()
    b := blood.UpdateReadingByID(reading)

    if b!=true {
        mutex.Unlock()
        msg:=fmt.Sprintf("updating reading with id and date %s, %s",reading.ID,reading.Date.Format(time.RFC3339))
        log.Print(msg)
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        e:=`{"error": "`+msg+`"}`+"\n"
        io.WriteString(w,e)
    } else {
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusAccepted)
        storedReading,bf:=blood.FindReadingByID(reading.ID) 
        if !bf {
            storedReading,bf=blood.FindReadingByID(origID)
        }
        mutex.Unlock()
        bs,err:=storedReading.ToJSON()
        if err==nil {
           w.Write(bs)
           io.WriteString(w,"\n")
        }
    }
}

func createReadingPUT(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    log.Printf("params=%v",params)
    var reading blood.BGReading
    _ = json.NewDecoder(r.Body).Decode(&reading)
    if reading.ID != params["id"] {
        log.Printf("ID in url and body not the same, overriding the body: %s %s.  Setting reading to URL ID",params["id"],reading.ID)
        reading.ID=params["id"]
    }
    createReading(reading,w)    
}

func createReading(reading blood.BGReading, w http.ResponseWriter) {
    mutex.Lock()
    error := blood.AddReading(reading)
    if error!=nil {
        mutex.Unlock()
        log.Printf("creating reading with id and date %s, %s",reading.ID,reading.Date.Format(time.RFC3339))
        log.Print(error.Error())
        
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotAcceptable)
        e:=`{"error": "`+error.Error()+`"}`+"\n"
        io.WriteString(w,e)
        
    } else {
       
        w.Header().Set("Content-Type", "application/json") 
        w.WriteHeader(http.StatusCreated)
        storedReading,_:=blood.FindReadingByID(reading.ID) // it was put without error so should be ok- in multithreaded process this wouldn't be true
        mutex.Unlock()
        bs,err:=storedReading.ToJSON()
        if err==nil {
           w.Write(bs)
           io.WriteString(w,"\n")
        }
    }
}


